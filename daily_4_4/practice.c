#define _CRT_SECURE_NO_WARNINGS 1
//写一个二分查找函数
//功能：在一个升序数组中查找指定的数值，找到了就返回下标，找不到就返回 - 1.
//int bin_search(int arr[], int left, int right, int key)
// arr 是查找的数组
//left 数组的左下标
//right 数组的右下标
//key 要查找的数字
#include <stdio.h>
int bin_search(int arr[], int left, int right, int key)
{
	
	while (left <= right)
	{
		int mid = left +(right -left) / 2;
		if (key < arr[mid])
		{
			right = mid - 1;
		}
		else if (key > arr[mid])
		{
			left = mid + 1;
		}
		else
			return mid;
	}
	return -1;
}
int main()
{
	int arr[10] = { 1 ,2 , 3, 4, 5, 6 ,7 ,8, 9 ,10 };
	int key = 0;
	while (scanf("%d", &key) != EOF)
	{
		int left = 0;
		int right = sizeof(arr) / sizeof(arr[0]) - 1;
		int r = bin_search(arr, left, right, key);
		if (r == -1)
			printf("找不到\n");
		else
			printf("找到了，下标是%d\n", r);
	}
	return 0;
}
