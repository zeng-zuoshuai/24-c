#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
//打印菱形
//######*
//#####***
//####*****
//###*******
//##*********
//#***********
//*************
// ***********
//  *********
//   *******
//    *****
//     ***
//      *
//int main()
//{
//	int line = 7;
//	//打印上半部分
//	int i = 0;
//	for (i = 0; i < line; i++)
//	{
//		int j = 0;
//		for (j = 0; j < line - 1 - i; j++)
//		{
//			printf(" ");
//		}
//		for (j = 0; j < i * 2 + 1 ; j++)
//		{
//			printf("*");
//		}
//		printf("\n");
//	}
//	//打印下半部分
//	for (i = 0; i < line - 1; i++)
//	{
//		int j = 0;
//		for (j = 0; j < i + 1; j++)
//		{
//			printf(" ");
//		}
//		for (j = 0; j < (line - i) * 2 - 3; j++)
//		{
//			printf("*");
//		}
//		printf("\n");
//	}
//
//	return 0;
//}递归和非递归分别实现求第n个斐波那契数
//
//例如：
//
//输入：5  输出：5
//
//输入：10， 输出：55
//
//输入：2， 输出：1
//递归
//int Fib(int n)
//{
//	if (n <= 2)
//	{
//		return 1;
//	}
//	else
//	{
//		return Fib(n - 1) + Fib(n - 2);
//	}
//}
//int main()
//{
//	int a = 1;
//	int b = 1;
//	int n = 6;
//	// 1 1 2 3 5 8 13
//	//int ret = Fib(n);
//	
//	while (n - 2)
//	{
//		int c = a + b;
//		a = b;
//		b = c;
//		n--;
//	}
//	//printf("%d\n",ret);
//	printf("%d\n", b);
//	return 0;
//}编写一个函数实现n的k次方，使用递归实现。
//int my_pow(int n, int k)
//{
//	if (k == 0)
//	{
//		return 1;
//	}
//	else
//		return n * my_pow(n, k - 1);
//}
//int main()
//{
//	int n = 5;
//	int k = 4;
//	int ret = my_pow(n, k);
//	printf("%d\n", ret);
//
//	return 0;
//}写一个递归函数DigitSum(n)，输入一个非负整数，返回组成它的数字之和
//
//例如，调用DigitSum(1729)，则应该返回1 + 7 + 2 + 9，它的和是19
//
//输入：1729，输出：19
//int DigitSum(int n)
//{
//	if (n / 10 == 0)//n < 10
//	{
//		return n;
//	}
//	else
//		return n % 10 + DigitSum(n / 10);
//}
//递归方式实现打印一个整数的每一位
//void DigitSum(int n)
//{
//	if (n < 10)
//		printf("%d ", n % 10);
//	else
//	{
//		DigitSum(n / 10);
//		printf("%d ", n % 10);
//	}
//}
//void DigitSum(int n)
//{
//	if (n > 9)
//	{
//		DigitSum(n / 10);
//	}
//	printf("%d ", n % 10);
//}
//int main()
//{
//	DigitSum(1729);
//	//printf("%d\n", ret);
//	return 0;
//}
//递归和非递归分别实现求n的阶乘（不考虑溢出的问题）
//int Fac(int n)
//{
//	if (n == 0)
//		return 1;
//	else
//		return n * Fac(n - 1);
//}
//int main()
//{
//	int n = 6;
//	// 1 2 3 4 5 6
//	//      720
//	int ret = 1;
//	/*while (n)
//	{
//		ret *= n;
//		n--;
//	}*/
//	for (int i = 2; i <= n; i++)
//	{
//		ret *= i;
//	}
//	//int ret = Fac(n);
//	printf("%d\n", ret);
//	return 0;
//}将一个字符串str的内容颠倒过来，并输出。
//输入
//I am a student
//输出
//tneduts a ma
//void Rev(char* str,int left, int right)
//{
//	while (left <= right)
//	{
//		char tmp = *(str + left);
//		*(str + left) = *(str + right);
//		*(str + right) = tmp;
//		left++;
//		right--;
//	}
//}
//实现一个函数，可以左旋字符串中的k个字符。
//
//例如：
//
//ABCD左旋一个字符得到BCDA
//
//ABCD左旋两个字符得到CDAB
//void Rot(char* str, int len, int k)
//{
//	int time = k % len;
//	for (int i = 0; i < time; i++)
//	{
//		int tmp = *str;
//		int j = 0;
//		for (j = 0; j < len - 1 ; j++)
//		{
//			*(str + j) = *(str + j + 1);
//		}
//		*(str + j) = tmp;
//	}
//}
//int main()
//{
//	//char str[10000] = {0};
//	//gets(str);
//	char str[] = "ABCD";
//	int k = 6;
//	int len = strlen(str);
//	int left = 0;
//	int right = len - 1;
//	printf("%s\n", str);
//	Rot(str, len, k);
//	printf("%s\n", str);
//	return 0;
//}
//int main()
//{
//	int a = 1;
//	int b = 2;
//	int c = (a > b, a = b + 10, a, b = a + 1);
//	printf("%d\n", c);
//	return 0;
//}
int main()
{
	int n = 1427;
	int a = n;
	int count = 0;
	int sum = 0;
	do
	{
		count++;
		n /= 10;
	} while (n);
	while (a)
	{
		sum += a % 10;
		a /= 10;
	}
	printf("%d %d\n", count, sum);
	return 0;
}