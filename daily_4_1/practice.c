#define _CRT_SECURE_NO_WARNINGS 1
//多组输入，一个整数（3~20），表示输出的行数，也表示组成正方形边的“ * ”的数量。
//输出描述：
//针对每行输入，输出用“ * ”组成的“空心”正方形，每个“ * ”后面有一个空格。
#include <stdio.h>
//int main()
//{
//	int n = 0;
//
//	while (scanf("%d", &n) != EOF)
//	{
//		 
//		for (int i = 0; i < n; i++)
//		{
//			for (int j = 0; j < n; j++)
//			{
//				if (i == 0 || i == n - 1 || j == 0 || j == n - 1)
//					printf("* ");
//				else
//					printf("  ");
//			}
//			printf("\n");
//		}
//	}
//
//	return 0;
//}
//第一行包含两个整数n和m，表示一个矩阵包含n行m列，用空格分隔。(1≤n≤10, 1≤m≤10)
//
//从2到n + 1行，每行输入m个整数（范围 - 231~231 - 1），用空格分隔，共输入n * m个数，表示第一个矩阵中的元素。
//
//输出描述：
//输出m行n列，为矩阵转置后的结果。每个数后面有一个空格。
//将数组A中的内容和数组B中的内容进行交换。（数组一样大）
//int main()
//{
//	int arr1[7] = { 1, 2, 3, 4, 5, 6, 7 };
//	int arr2[7] = { 11,22,33, 44, 45, 66,77 };
//	int tmp = 0;
//	for (int i = 0; i < 7; i++)
//	{
//		tmp = arr1[i];
//		arr1[i] = arr2[i];
//		arr2[i] = tmp;
//	}
//	for (int i = 0; i < 7; i++)
//	{
//		printf("%d ", arr1[i]);
//	}
//	printf("\n");
//	for (int i = 0; i < 7; i++)
//	{
//		printf("%d ", arr2[i]);
//	}
//	return 0;
//}多组输入，一个整数（2~20），表示输出的行数，也表示组成“X”的反斜线和正斜线的长度。
//int main()
//{
//	int n = 0;
//	while (scanf("%d", &n) != EOF)
//	{
//		for (int i = 1; i <= n; i++)
//		{
//			for (int j = 1; j<=n; j++)
//			{
//				if ((i + j) == (n + 1)||(i==j))
//					printf("*");
//				else
//					printf(" ");
//			}printf("\n");
//		}
//	}
//	return 0;
//}
