#define _CRT_SECURE_NO_WARNINGS 1
//写一个函数，判断一个字符串是否为另外一个字符串旋转之后的字符串。
//例如：给定s1 = AABCD和s2 = BCDAA，返回1
//给定s1 = abcd和s2 = ACBD，返回0.
//AABCD左旋一个字符得到ABCDA
//AABCD左旋两个字符得到BCDAA
//AABCD右旋一个字符得到DAABC
#include<stdio.h>
#include<string.h>
//int Jub(char* s1, char* s2, size_t len)
//{
//	int a = 0;
//	for (int i = 0; i < len; i++)
//	{
//		//char* s3 = s1 + i;
//		//*(p+i)
//		if (strcmp(s1 + i, s2) == 0)
//		{
//			a = 1;
//			break;
//		}
//	}
//	return a;
//}
//int main()
//{
//	char* s1 = "AABCD";
//	char* s2 = "BCDAA";
//	size_t len = strlen(s1);
//	int ret = Jub(s1, s2 ,len);
//	if (ret)
//	{
//		printf("yes\n");
//	}
//	else
//		printf("No\n");
//	return 0;
//}多组输入，一行有两个整数，分别表示年份和月份，用空格分隔。
//int main()
//{
//	int year = 0;
//	int month = 0;
//	
//	int arr[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
//	
//	while (scanf("%d%d", &year, &month) != EOF)
//	{
//		int day = arr[month];
//		if (((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0))&&month==2)
//		{
//			day += 1;
//		}
//		printf("%d\n", day);
//	}
//	return 0;
//}一个数组中只有两个数字是出现一次，其他所有数字都出现了两次。
//编写一个函数找出这两个只出现一次的数字。
//例如：
//有数组的元素是：1，2，3，4，5，1，2，3，4，6
//只有5和6只出现1次，要找出5和6.
//void Seek(int* p, int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		int j = 0;
//		for (j = 1; i+ j < sz; j++)
//		{
//			if (*(p + i) == *(p + i + j))
//				break;
//			else
//				printf("%d\n", *(p + i));
//		}
//	}
//}
//int main()
//{
//	int arr[] = { 1,2,3,4,5,1,2,3,4,6 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	Seek(arr, sz);
//	return 0;
//}编写一个函数，实现两个整数的交换，要求采用指针的方式实现。
//输入描述：
//键盘输入2个整数 m 和 n
//输出描述：
//输出交换后m 和 n 的值，中间使用空格隔开
//示例1输入：
//2
//3
//复制
//输出：
//3 2
//void Typ(int* p1, int* p2)
//{
//	int tmp = 0;
//	tmp = *p1;
//	*p1 = *p2;
//	*p2 = tmp;
//}
//int main()
//{
//	int a = 0;
//	int b = 0;
//
//	scanf("%d%d", &a, &b);
//	int* p1 = &a;
//	int* p2 = &b;
//	Typ(p1, p2);
//	printf("%d %d", a, b);
//	return 0;
//}键盘输入一个字符串，编写代码获取字符串的长度并输出，要求使用字符指针实现
#include<stdio.h>
#include<string.h>
//#include<stdio.h>
//int main()
//{
//	char ch[100000] = {0};
//	scanf("%[^\n]", &ch);
//	size_t len = strlen(ch);
//	printf("%d\n", len);
//
//	return 0;
//}
void Typ(char* p, size_t len)
{
	int i = 0;
	for (i = 0; i < len / 2; i++)
	{
		int tmp = 0;
		tmp = *(p + i);
		*(p + i) = *(p + len - 1 - i);
		*(p + len - 1 - i) = tmp;
	}
}
void Pint(char* p, size_t len)
{
	for (int i = 0; i < len; i++)
	{
		printf("%c", *(p + i));
		//printf("%c",i[p]);
	}
}
int main()
{
	char ch[10000] = { 0 };
	scanf("%[^\n]", ch);
	char* p = ch;
	size_t len = strlen(ch);
	/*for (int i = 0; i < len; i++)
	{
		printf("%c", *(p + len - 1 - i));
	}*/
	Typ(p, len);
	Pint(p, len);
	return 0;
}