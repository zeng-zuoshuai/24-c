#define _CRT_SECURE_NO_WARNINGS 1
//将课堂上所讲使用函数指针数组实现转移表的代码，
//自己实践后，并提交代码到答案窗口。

//#include <stdio.h>
//
//void menu()
//{
//	printf("***************************\n");
//	printf("*******1.Add   2.Sub*******\n");
//	printf("*******3.Mul   4.Div*******\n");
//	printf("*******0.exit       *******\n");
//	printf("***************************\n");
//}
//
//int Add(int x, int y)
//{
//	return x + y;
//}
//int Sub(int x, int y)
//{
//	return x - y;
//}
//int Mul(int x, int y)
//{
//	return x * y;
//}
//int Div(int x, int y)
//{
//	return x / y;
//}
//
//void Calc(int (*pf)(int, int))
//{
//	int x = 0;
//	int y = 0;
//	printf("请输入操作数:");
//	scanf("%d%d", &x, &y);
//	int ret = pf(x, y);
//	printf("%d\n", ret);
//}
//int main()
//{
//	int input = 0;
//	do
//	{
//		menu();
//		printf("请选择>");
//		scanf("%d", &input);
//		switch (input)
//		{
//		case 1 :
//			Calc(Add);
//			break;
//		case 2 :
//			Calc(Sub);
//			break;
//		case 3:
//			Calc(Mul);
//			break;
//		case 4:
//			Calc(Div);
//			break;
//		case 0 :
//			printf("程序退出\n");
//			break;
//		default :
//			printf("输入错误，请重新选择\n");
//			break;
//		}
//	} while (input);
//
//	return 0;
//}
//一个数组中只有两个数字是出现一次，其他所有数字都出现了两次。
//
//编写一个函数找出这两个只出现一次的数字。
//
//例如：
//
//有数组的元素是：1，2，3，4，5，1，2，3，4，6
//
//只有5和6只出现1次，要找出5和6.


//1, 2, 3, 4, 5, 1, 2, 3, 4, 6
//void Find(int arr[], int sz)
//{
//	int i = 2;
//	while (i < sz)
//	{
//		int s = i;
//		int k = arr[i];
//		int flag = 1;
//		int j = 0;
//		for (j = 1; j < sz; j++)
//		{
//			if (arr[j] == k)
//			{
//				flag = 0;
//			}
//		}
//		if (flag)
//		{
//			int tmp = arr[s];
//			for (; s < sz - 1; s++)
//			{
//				arr[s] = arr[s + 1];
//			}
//			arr[0] = tmp;
//			i--;
//		}
//		i++;
//	}
//	/*for (i = 0; i < sz; i++)
//	{
//		if (arr[i] != 0)
//		{
//			printf("%d ", arr[i]);
//		}
//	}*/
//}

int main()
{
	int arr[] = { 1, 2, 3, 4, 5, 1, 2, 3, 4, 6 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	//Find(arr, sz);
	printf("%d %d\n", arr[0], arr[1]);
	return 0;
}
#include <stdio.h>