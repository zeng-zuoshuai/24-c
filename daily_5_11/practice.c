#define _CRT_SECURE_NO_WARNINGS 1
//模拟实现库函数strlen
#include<stdio.h>

//int Mystrlen(char* p)
//{
//	int count = 0;
//	char* pa = p;
//	char* pb = pa;
//	while (*pb != '\0')
//	{
//		count++;
//		pb++;
//	}
//	return count;
//}
//int main()
//{
//	char ch[] = "abcdefgh";
//	//int* p = arr;
//	printf("%s\n", ch);
//	int len =Mystrlen(ch);
//	printf("%d\n", len);
//	return 0;
//}调整数组使奇数全部都位于偶数前面。
//题目：
//输入一个整数数组，实现一个函数，
//来调整该数组中数字的顺序使得数组中所有的奇数位于数组的前半部分，
//所有偶数位于数组的后半部分。
void Sort(int arr[], int sz)
{
	int* i = arr;
	int* j = arr + sz - 1;
	while (i < j)
	{
		while (i < j && *i % 2 != 0)
		{
			i++;
		}
		while (i < j && *j % 2 == 0)
		{
			j--;
		}
		int tmp = *i;
		*i = *j;
		*j = tmp;
	}
}
int main()
{
	int arr[] = { 1,34,6,74,2,6,83,3,7,9 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	Sort(arr, sz);
	for (int i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}
	return 0;
}