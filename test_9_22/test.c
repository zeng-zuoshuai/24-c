#define _CRT_SECURE_NO_WARNINGS 1

//#include <stdio.h>
//#include <stdlib.h>
//int main()
//{
//	int** p = (int**)malloc(3 * sizeof(int*));
//	int i = 0;
//	for (i = 0; i < 3; i++)
//	{
//		p[i] = (int*)malloc(5 * sizeof(int));
//	}
//	int j = 0;
//	for (i = 0; i < 3; i++)
//	{
//		for (j = 0; j < 5; j++)
//		{
//			p[i][j] = 5 * i + j;
//		}
//	}
//	for (i = 0; i < 3; i++)
//	{
//		for (j = 0; j < 5; j++)
//		{
//			printf("%d ", p[i][j]);
//		}
//		printf("\n");
//	}
//	for (i = 0; i < 3; i++)
//	{
//		free(p[i]);
//	}
//	free(p);
//	p = NULL;
//	return 0;
//}

#include <stdio.h>
#include <stdlib.h>

int main()
{
	int arr[10] = { 0 };
	//int* ptr = (int*)malloc(5 * sizeof(int));
	int* ptr = (int*)calloc(5, sizeof(int));
	
	if (ptr != NULL)
	{		
		int i = 0;
		/*for (i = 0; i < 5; i++)
		{
			*(ptr + i) = i;
		}*/
		int* p = (int*)realloc(ptr, 10 * sizeof(int));
		if (p != NULL)
		{
			ptr = p;
			for (i = 0; i < 10; i++)
			{
				printf("%d ", *(ptr + i));
			}
		}		
		free(ptr);
		ptr = NULL;
	}
	
	return 0;
}