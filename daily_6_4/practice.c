#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <assert.h>
#include <string.h>

//模拟实现strncpy
//char* my_strncpy(char* dest, const char* src, int n)
//{
//	char* ret = dest;
//	assert(dest && src);
//	while (n--)
//	{
//		*dest = *src;
//		dest++;
//		src++;
//	}
//	return ret;
//}
//int main()
//{
//	char arr1[] = "abcdef";
//	char arr2[20] = { 0 };
//	char* ret = my_strncpy(arr2, arr1, 4);
//	printf("%s\n", arr2);
//	return 0;
//}
//模拟实现strncat
//char* my_strncat(char* dest, const char* src, int n)
//{
//	char* ret = dest;
//	assert(dest && src);
//	while (*dest)
//	{
//		dest++;
//	}
//		while (n--)
//	{
//		*dest = *src;
//		dest++;
//		src++;
//	}
//	return ret;
//
//}
//int main()
//{
//	char arr1[] = "abcdef";
//	char arr2[20] = "efghi";
//	char* ret = my_strncat(arr2, arr1, 4);
//	printf("%s\n", arr2);
//	return 0;
//}
//模拟实现memcpy
//void* my_memcpy(void* dest, const void* src, size_t num)
//{
//	void* ret = dest;
//	assert(dest && src);
//	while (num--)
//	{
//		*(char*)dest = *(char*)src;
//		dest = (char*)dest + 1;
//		// ((char*)dest)++;
//		src = (char*)src + 1;
//	}
//	return ret;
//}
//int main()
//{
//	int arr1[] = { 1,2,3,4,5 };
//	int arr2[20] = { 0 };
//	my_memcpy(arr2, arr1, 12);
//	int i = 0;
//	for (i = 0; i < 20; i++)
//	{
//		printf("%d ", arr2[i]);
//	}
//	return 0;
//}
//模拟实现memmove
//void* my_memcpy(void* dest, const void* src, size_t num)
//{
//	void* ret = dest;
//	assert(dest && src);
//	if (dest < src)
//	{
//		//前到后
//		while (num--)
//		{
//			*(char*)dest = *(char*)src;
//			dest = (char*)dest + 1;
//		    // ((char*)dest)++;
//			src = (char*)src + 1;
//		}
//	}
//	else
//	{
//		while (num--)
//		{
//			*((char*)dest + num) = *((char*)src + num);
//		}
//	}
//	return ret;
//}
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	my_memcpy(arr, arr + 2, 20);
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}
//写一个函数判断当前机器是大端还是小端，如果是小端返回1，如果是大端返回0.
int Jab()
{
	int i = 1;
	return (*(char*)&i);
}
int main()
{
	int ret = Jab();
	if (ret == 1)
	{
		printf("小端\n");
	}
	else
	{
		printf("大端\n");
	}
	return 0;
}