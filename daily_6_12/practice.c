#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
//#pragma pack(8)
//struct stu//结构体声明
//{
//	char name[20];
//	int age;
//	char sex[5];
//	char id[20];
//};
//struct A//位段声明
//{
//	int _a : 2;
//	int _b : 5;
//	int _c : 10;
//	int _d : 30;
//};
//struct A b = { ._a = 1000 };
//enum Color
//{
//	RED = 1,
//	GREEN = 2,
//	BLUE = 4
//};
//enum Color clr = RED;
//int sys()
//{
//	union
//	{
//		int i;
//		char c;
//	}un,uc;
//	un.i = 1;
//	uc.c = 1;
//	//union Un2 s = { .i = 1 };
//	return un.c;
//}
//int main()
//{
//	//struct stu s = { "张三" };
//	//printf("%d", s.age);
//
//	//struct stu s2 = {.age = 20};
//	//printf("%d", s2.age);
//	union Un//联合体声明
//	{
//		char c;
//		int i;
//	};
//	union Un s = { .c = 0 };
//	struct A a = { ._b = 20 };
//	a._a = 10;
//	return 0;
//}
//#pragma pack();
//typedef struct Stu
//{
//	int i;
//	char c;
//}Stu;
//Stu s = { .c = 'x' };
//#include <stdlib.h>
//
//int main()
//{
//	int* ptr = (int*)malloc(20);
//	if (ptr != NULL)
//	{
//		
//	}
//	free(ptr);
//	ptr = NULL;
//	int* ptr2 = (int*)calloc(5, 4);
//	if (ptr2 != NULL)
//	{
//		
//	}
//	int* p = (int*)realloc(ptr2, 40);
//	if (p != NULL)
//	{
//		ptr2 = p;
//	}
//
//	free(ptr2);
//	ptr2 = NULL;
//	return 0;
//}
//使用malloc函数模拟开辟一个3*5的整型二维数组，
// 开辟好后，使用二维数组的下标访问形式，访问空间。
#include <stdlib.h>

int main()
{
	
	int* p = (int*)calloc(3 * 5 , sizeof(int));
	printf("%d", p[3]);
	return 0;
}