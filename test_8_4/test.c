#define _CRT_SECURE_NO_WARNINGS 1

//练习使用库函数，qsort排序各种类型的数据.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct Stu 
{
	char name[10];
	int age;
};

int cmp_stu_by_age(const void* e1, const void* e2)
{
	return (*(struct Stu*)e1).age - (*(struct Stu*)e2).age;
}
int cmp_stu_by_name(const void* e1, const void* e2)
{
	return strcmp((*(struct Stu*)e1).name, (*(struct Stu*)e2).name);
}

void Swap(char* buf1, char* buf2, size_t width)
{
	int i = 0;
	for (i = 0; i < width; i++)
	{
		char tmp = *buf1;
		*buf1 = *buf2;
		*buf2 = tmp;
		buf1++;
		buf2++;
	}
}
void bubble_sort(void* dest, size_t num, size_t width, int (*cmp)(const void* e1, const void* e2))
{
	int i = 0;
	for (i = 0; i < num - 1; i++)
	{
		int j = 0;
		for (j = 0; j < num - 1 - i; j++)
		{
			if (cmp((char*)dest + j * width, (char*)dest + (j + 1) * width) > 0)
			{
				Swap((char*)dest + j * width, (char*)dest + (j + 1) * width, width);
			}
		}
	}
}
int main()
{
	struct Stu s[] = { {"zhangsan", 18}, 
		{"lisi", 26}, {"wamgwu", 15} };

	int sz = sizeof(s) / sizeof(s[0]);
	bubble_sort(s, sz, sizeof(s[0]), cmp_stu_by_name);
	return 0;
}