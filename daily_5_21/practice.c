#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
//一个数组中只有两个数字是出现一次，其他所有数字都出现了两次。
//
//编写一个函数找出这两个只出现一次的数字。
//
//例如：
//
//有数组的元素是：1，2，3，4，5，1，2，3，4，6
//              1,2,3,4,5     1 2 3 4 6
//只有5和6只出现1次，要找出5和6.
//void Find(int* arr,int sz)//int* arr
//{
//	int arr2[20] = { 0 };
//	//strcpy(arr2, arr);
//	
//	//遍历
//	for (int i = 0; i < sz ; i++)
//	{
//		int* s1 = arr + i;
//		int* s2 = s1 + 1;
//
//		while (*s2 != '\0')
//		{
//			if (*s1 == *s2)
//				printf("%c\n", *s1);
//			s2++;
//		}
//		
//
//	}
//
//}
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,1,2,3,4,6 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	Find(arr,sz);
//	return 0;
//}
//写代码将三个整数数按从大到小输出。
// 
//例如：
//
//输入：2 3 1
//
//输出：3 2 1
//int main()
//{
//	int a = 0;
//	int b = 0;
//	int c = 0;
//	int arr[3] = { 0 };
//	for (int i = 0; i < 3; i++)
//	{
//		scanf("%d", &arr[i]);
//	}
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	// 3 2 1 3 4 5
//	for (int i = 0; i < sz - 1; i++)
//	{
//		int j = 0;
//		for (j = 0; j < sz - 1 - i; j++)
//		{
//			if (arr[j] < arr[j + 1])
//			{
//				int tmp = arr[j];
//				arr[j] = arr[j + 1];
//				arr[j + 1] = tmp;
//			}
//		}
//
//	}
//	for (int i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}
//写代码将三个整数数按从大到小输出。
//
//例如：
//
//输入：2 3 1
//     3 2 4
//输出：3 2 1
//int main()
//{
//	int a = 2;
//	int b = 3;
//	int c = 1;
//	scanf("%d%d%d", &a, &b, &c);
//	if (a < b)
//	{
//		int tmp = a;
//		a = b;
//		b = tmp;
//	}
//	if (a < c)
//	{
//		int tmp = a;
//		a = c;
//		c = tmp;
//	}
//	if (b < c)
//	{
//		int tmp = b;
//		b = c;
//		c = tmp;
//	}
//	printf("%d %d %d", a, b, c);
//	return 0;
//}
//int main()
//{
//	int a = 2;
//	int b = 3;
//	int c = 1;
//	scanf("%d%d%d", &a, &b, &c);
//	if (a < b)
//	{
//		int tmp = a;
//		a = b;
//		b = tmp;
//	}
//	if (a < c)
//	{
//		int tmp = c;
//		c = b;
//		b = a;
//		a = tmp;
//
//	}
//	else if (b < c && a > c)
//	{
//		int tmp = b;
//		b = c;
//		c = tmp;
//
//	}
//	else
//	{
//		;
//	}
//	printf("%d %d %d", a, b, c);
//	return 0;
//}编写程序数一下 1到 100 的所有整数中出现多少个数字9
//int main()
//{
//	int i = 0;
//	int count = 0;
//	for (i = 1; i <= 100; i++)
//	{
//		// 9 19 29 ... 89 90 91 92...99
//		if (i % 10 == 9)
//		{
//			count++;
//		}
//		if (i / 10 == 9)
//		{
//			count++;
//		}
//	}
//	printf("%d\n", count);
//	return 0;
//KiKi想知道已经给出的三条边a，b，c能否构成三角形，
//如果能构成三角形，判断三角形的类型（等边三角形、等腰三角形或普通三角形）。
//输入描述：
//题目有多组输入数据，每一行输入三个a，b，c(0 < a, b, c < 1000)，作为三角形的三个边，用空格分隔。
//输出描述：
//针对每组输入数据，输出占一行，如果能构成三角形，
//等边三角形则输出“Equilateral triangle!”，
//等腰三角形则输出“Isosceles triangle!”，
//其余的三角形则输出“Ordinary triangle!”，反之输出“Not a triangle!”。
//int main()
//{
//	int a = 0;
//	int b = 0;
//	int c = 0;
//	while (scanf("%d%d%d", &a, &b, &c) != EOF)
//	{
//		if (a + b > c && a + c > b && b + c > a)
//		{
//			if (a == b && b == c)
//			{
//				printf("Equilateral triangle!\n");
//			}
//			else if ((a == b && b != c) || (a == c && c != b) || (b == c && a != b))
//			{
//				printf("Isosceles triangle!\n");
//			}
//			else
//			{
//				printf("Ordinary triangle!\n");
//			}
//		}
//		else
//		{
//			printf("Not a triangle!\n");
//		}
//	}
//	return 0;
//}
//在屏幕上输出9 * 9乘法口诀表
//int main()
//{
//	int i = 0;
//	for (i = 1; i <= 9; i++)
//	{
//		int j = 0;
//		for (j = 1; j <= i; j++)
//		{
//			printf("%d*%d==%2d ", j, i, i * j);
//		}
//		printf("\n");
//	}
//
//	return 0;
//}
// 求10 个整数中最大值
//int main()
//{
//	int arr[10] = { 1, 2, 5, 7, 8,10, 12,3,6,9 };
//	for (int i = 0; i < 9; i++)
//	{
//		if (arr[i] > arr[i + 1])
//		{
//			int tmp = arr[i];
//			arr[i] = arr[i + 1];
//			arr[i + 1] = tmp;
//		}
//	}
//	printf("%d\n", arr[9]);
//	return 0;
//}
//int main()
//{
//	int arr[10] = { 1, 2, 5, 7, 8,10, 12,3,6,9 };
//	int max = arr[0];
//	for (int i = 1; i <= 9; i++)
//	{
//		if (arr[i] > max)
//		{
//			max = arr[i];
//		}
//	}
//	printf("%d\n", max);
//	return 0;
//}
//计算1 / 1 - 1 / 2 + 1 / 3 - 1 / 4 + 1 / 5 …… + 1 / 99 - 1 / 100 的值，打印出结果
// float  double  long double
//  f        lf      Lf
//  f         f      Lf
//int main()
//{
//	double sum = 0.0;
//	int flag = 1;
//	for (int i = 1; i <= 100; i++)
//	{
//		sum += 1.0 / i * flag;
//		flag = -flag;
//	}
//	printf("%f\n", sum);
//}
//写一个代码：打印100~200之间的素数
int main()
{
	//产生100~200 的数字
	
	for (int i = 100; i <= 500; i++)
	{
		int flag = 1;
		// 55
		//产生2~i-1之间的数
		int j = 2;
		for (j = 2; j <= i-1; j++)
		{
			// 3
			//试除
			if (i % j == 0)
			{
				flag = 0;
				break;
			}
			

		}
		if (flag)
		{
			printf("%d ", i);
		}
	}
	return 0;
}
// 猜数字游戏
