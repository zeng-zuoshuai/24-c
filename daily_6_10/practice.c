#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <math.h>
//int main() {
//    int i = 0;
//    for (i = 10000; i <= 99999; i++)
//    {
//        //12345
//        //1*2345+12*345+123*45+1234*5
//        int sum = 0;
//        int j = 0;
//        for (j = 1; j <= 4; j++)
//        {
//            int a = pow(10, j);
//            sum += (i / a) * (i % a);
//        }
//        if (sum == i)
//            printf("%d ", sum);
//
//    }
//    return 0;
//}
//使用联合体的知识，写一个函数判断当前机器是大端还是小端，
// 如果是小端返回1，如果是大端返回0.
//int Func()
//{
//	union
//	{
//		int i;
//		char u;
//	}um;
//	um.i = 1;
//	return um.u;
//}
//int main()
//{
//	int ret = Func();
//	if (Func)
//		printf("小端\n");
//	else
//		printf("大端\n");
//	return 0;
//}
//使用malloc函数模拟开辟一个3*5的整型二维数组，
// 开辟好后，使用二维数组的下标访问形式，访问空间。
#include <stdlib.h>
//int main()
//{
//	int arr[3][5] = { 0 };
//	int* p = arr[0];
//	p = (int*)malloc(3 * 5 * sizeof(int));
//	if (p == NULL)
//	{
//		perror("malloc");
//	}
//	int i = 0;
//	for (i = 0; i < 15; i++)
//	{
//		*(p + i) = i + 1;
//	}
//	int j = 0;
//	for (i = 0; i < 3; i++)
//	{
//		for (j = 0; j < 5; j++)
//		{
//			printf("%d ", p[i][j]);
//		}
//		printf("\n");
//	}
//	free(p);
//	p = NULL;
//	return 0;
//}
//int main()
//{
//    int n = 0;
//    scanf("%d", &n);
//    int nums[100];
//    int i = 0;
//    for (i = 0; i < n; i++)
//    {
//        scanf("%d", &nums[i]);
//    }
//    int ans[2 * 100];
//    for (i = 0; i < 2 * n; i++)
//    {
//        if (i < n)
//            ans[i] = nums[i];
//        else
//            ans[i] = nums[i - n];
//        printf("%d ", ans[i]);
//    }
//    return 0;
//}
//模拟实现strstr，字符串找子串
#include <assert.h>
//char* my_strstr(const char* str1, const char* str2)
//{
//	if (str2 == NULL)
//		return (char*)str1;
//	assert(str1 && str2);
//	char* cur = (char*)str1;
//	while (*cur)
//	{
//		char* s1 = cur;
//		char* s2 = (char*)str2;
//		while (*s1 == *s2 && *s1 && *s2)
//		{
//			s1++;
//			s2++;
//		}
//		if (*s2 == '\0')
//			return cur;
//		cur++;
//	}
//	return NULL;
//}
//int main()
//{
//	char arr1[] = "hello world";
//	char arr2[] = "";
//	char* ret = my_strstr(arr1, arr2);
//	if (ret)
//		printf("%s\n", ret);
//	else
//		printf("找不到\n");
//
//	return 0;
//}
//模拟实现strlen
//size_t my_strlen(const char* s)
//{
//	char* p = (char*)s;
//	while (*p != '\0')
//	{
//		p++;
//	}
//	return p - s;
//}
//size_t my_strlen(char* p)
//{
//	if (!*p)
//		return 0;
//	else
//		return 1 + my_strlen(p + 1);
//}
//size_t my_strlen(char* p)
//{
//	size_t count = 0;
//	while (*p)
//	{
//		count++;
//		p++;
//	}
//	return count;
//}
//int main()
//{
//	char arr[] = "abcdef";
//	size_t ret = my_strlen(arr);
//	printf("%zd\n", ret);
//	return 0;
//}
//模拟实现strcpy
//char* my_strcat(char* dest, const char* src)
//{
//	char* ret = dest;
//	assert(dest && src);
//	while (*src != '\0')
//	{
//		*dest++ = *src++;
//	}
//	return ret;
//}
//char* my_strcat(char* dest, const char* src)
//{
//	char* ret = dest;
//	assert(dest && src);
//	while (*dest++ = *src++);
//	return ret;
//}
//int main()
//{
//	char arr[] = "bcdef";
//	char arr2[20] = { 0 };
//	char* ret = my_strcat(arr2, arr + 1);
//	printf("%s\n", ret);
//	return 0;
//}
//模拟实现strcat
//char* my_strcat(char* dest, const char* src)
//{
//	char* ret = dest;
//	assert(dest && src);
//	while (*dest)
//		dest++;
//	while (*dest++ = *src++);
//	return ret;
//}
//int main()
//{
//	char arr1[] = "abcd";
//	char arr2[20] = "efdhi";
//	char* ret = my_strcat(arr2, arr1);
//	printf("%s\n", ret);
//	return 0;
//}
//模拟实现strcmp
//int my_strcmp(const char* dest, const char* src)
//{
//	assert(dest && src);
//	while (*dest == *src)
//	{
//		if (!*dest)
//			return 0;
//		dest++;
//		src++;
//	}
//	if (*dest > *src)
//		return 1;
//	else
//		return - 1;
//}
//int my_strcmp(const char* dest, const char* src)
//{
//	assert(dest && src);
//	while (*dest == *src)
//	{
//		if (!*dest)
//			return 0;
//		dest++;
//		src++;
//	}
//	return *dest - *src;
//}
//int main()
//{
//	char arr1[] = "abcd";
//	char arr2[20] = "abq";
//	int ret = my_strcmp(arr2, arr1);
//	printf("%d\n", ret);
//	return 0;
//}
#include <assert.h>
//模拟实现memcpy
//void* my_memcpy(void* dest, const void* src, size_t num)
//{
//	void* ret = dest;
//	assert(dest && src);
//	while (num--)
//	{
//		*(char*)dest = *(char*)src;
//		//((char*)dest)++;
//		//((char*)src)++;
//		dest = (char*)dest + 1;
//		src = (char*)src + 1;
//	}
//	return ret;
//}
//memmove
void* my_memmove(void* dest, void* src, size_t num)
{
	void* ret = dest;
	assert(dest && src);
	if (dest < src)
	{
		//前-->后
		while (num--)
		{
		*(char*)dest = *(char*)src;
		//((char*)dest)++;
		//((char*)src)++;
		dest = (char*)dest + 1;
		src = (char*)src + 1;
		}
	}
	else
	{
		//后-->前
		while (num--)
		{
			*((char*)dest + num) = *((char*)src + num);
		}
	}
	return ret;
}
int check_sys()
{
	int i = 1;
	return (*(char*)&i);
}
int main()
{
	int arr[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
	//             
	int* ret = (int*)my_memmove(arr + 2, arr, 20);

	return 0;
}