#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

//void test(int arr[])
//{
//	int sz = sizeof(arr);
//	printf("%d\n", sz);
//}
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,89,10 };
//
//	test(arr);
//	return 0;
//}

//写一个函数打印arr数组的内容，不使用数组下标，使用指针。
//
//arr是一个整形一维数组。
//void test(int arr[], int sz)//升序
//{
//	int i = 0;
//	int j = 0;
//	for (i = 0; i < sz - 1; i++)
//	{
//		for (j = 0; j < sz - 1 - i; j++)
//		{
//			if (arr[j] > arr[j + 1])
//			{
//				int tmp = arr[j];
//				arr[j] = arr[j + 1];
//				arr[j + 1] = tmp;
//			}
//		}
//	}
//}
//void Print(int* p, int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", *(p + i));
//	}
//}
//int main()
//{
//	int arr[] = { 7,3,2,1,5,6,7,8,9,0 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//
//	test(arr, sz);
//	Print(arr, sz);
//	return 0;
//}
//写一个函数，判断一个字符串是否为另外一个字符串旋转之后的字符串。
//例如：给定s1 = AABCD和s2 = BCDAA，返回1
//
//给定s1 = abcd和s2 = ACBD，返回0.
//AABCD左旋一个字符得到ABCDA
//
//AABCD左旋两个字符得到BCDAA
//
//AABCD右旋一个字符得到DAABC
#include <string.h>
int test(char* s1, char* s2, int len)
{
	if (strcmp(s1, s2) == 0)
		return 1;
	int i = 0;
	//abcd
	//abc bca cab 
	
	for (i = 0; i < len - 1; i++)
	{
		int j = 0;
		char tmp = *s2;
		for (j = 0; j < len - 1; j++)
		{
			
			*(s2 + j) = *(s2 + j + 1);

		}
		*(s2 + j) = tmp;
		if (strcmp(s1, s2) == 0)
			return 1;
	}
	return 0;
}
int main()
{
	char arr[] = "ABCDEF";
	char arr2[] = "FABCDE";
	int len = strlen(arr);
	int r = test(arr, arr2, len);
	printf("%d\n", r);
	return 0;
}
//有一个数字矩阵，矩阵的每行从左到右是递增的，
//矩阵从上到下是递增的，
//请编写程序在这样的矩阵中查找某个数字是否存在。
//要求：时间复杂度小于O(N);

//int main()
//{
//	int arr[3][3] = { {1,2,3}, 
//                      {4,5,6}, 
//		              {7,8,9} };
//	int k = 7;
//	int i = 0;
//	int flag = 0;
//	int j = 3 - 1;
//	while (arr[i][j] < k && i < 3)
//		i++;
//	if (arr[i][j] > k)
//	{
//		for (j = 3 - 1; j >= 0; j--)
//		{
//			if (arr[i][j] == k)
//			{
//				flag = 1;
//				break;
//			}
//		}
//	}
//	printf("%d\n", flag);
//	return 0;
//}
//日本某地发生了一件谋杀案，警察通过排查确定杀人凶手必为4个嫌疑犯的一个。
//
//以下为4个嫌疑犯的供词:
//
//A说：不是我。
//
//B说：是C。
//
//C说：是D。
//
//D说：C在胡说
//
//已知3个人说了真话，1个人说的是假话。
//
//现在请根据这些信息，写一个程序来确定到底谁是凶手。

//int main()
//{
//	char k = 0;
//	for (k = 'A'; k <= 'D'; k++)
//	{
//		if ((k != 'A') + (k == 'C') + (k == 'D') + (k != 'D') == 3)
//			printf("%c\n", k);
//	}
//	return 0;
//}
//在屏幕上打印杨辉三角。
//1
//
//1 1
//
//1 2 1
//
//1 3 3 1
//int main()
//{
//	int arr[10][10] = { 0 };
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		int j = 0;
//		for (j = 0; j <= i; j++)
//		{
//			arr[i][0] = 1;
//			if (j != 0)
//			{
//				arr[i][j] = arr[i - 1][j] + arr[i - 1][j - 1];
//			}
//		}
//	}
//	for (i = 0; i < 10; i++)
//	{
//		int j = 0;
//		for (j = 0; j <= i; j++)
//		{
//			printf("%3d ", arr[i][j]);
//		}
//		printf("\n");
//	}
//	return 0;
//}