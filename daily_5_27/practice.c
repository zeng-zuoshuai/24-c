#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
//杨辉三角
// 1
// 1 1
// 1 2 1
// 1 3 3 1
// 1 4 6 4 1
//void YangH(int n)
//{
//	int arr[30] = { 1 };
//	printf("1\n");
//	int i = 0;
//	int j = 0;
//	for (i = 1; i < n; i++)
//	{
//		for (j = i; j > 0; j--)
//		{
//			arr[j] += arr[j - 1];
//		}
//		for (j = 0; j <= i; j++)
//		{
//			printf("%d ", arr[j]);
//		}
//		printf("\n");
//	}
//}
//int main()
//{
//	YangH(5);
//	return 0;
//}
//调整数组使奇数全部都位于偶数前面。
//
//题目：
//
//输入一个整数数组，实现一个函数，
//
//来调整该数组中数字的顺序使得数组中所有的奇数位于数组的前半部分，
//
//所有偶数位于数组的后半部分。
//void Adj(int * p, int sum)
//{
//	int left = 0;
//	int right = sum - 1;
//	while (left < right)
//	{
//		while (*(p + left) % 2 == 1 && left < right)
//		{
//			left++;
//		}
//		while (*(p + right) % 2 == 0 && left < right)
//		{
//			right--;
//		}
//		if (left < right)
//		{
//			int tmp = *(p + left);
//			*(p + left) = *(p + right);
//			*(p + right) = tmp;
//		}
//	}
//}
//void Pint(int* p, int sum)
//{
//	int i = 0;
//	for (i = 0; i < sum; i++)
//	{
//		printf("%d ", *(p + i));
//	}
//	printf("\n");
//}
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,0 };
//	int sum = sizeof(arr) / sizeof(arr[0]);
//	Pint(arr, sum);
//
//	Adj(arr, sum);
//
//	Pint(arr, sum);
//	return 0;
//}
//将课堂上所讲使用函数指针数组实现转移表的代码，自己实践后，并提交代码到答案窗口。
int Add(int x, int y)
{
	return x + y;
}
int Sub(int x, int y)
{
	return x - y;
}
int Mul(int x, int y)
{
	return x * y;
}
int Div(int x, int y)
{
	return x / y;
}
void menu()
{
	printf("***********************\n");
	printf("*****1.Add   2.Sub*****\n");
	printf("*****3.Mul   4.Div*****\n");
	printf("******   0.exit    ****\n");
	printf("***********************\n");
	printf("请选择：<\n");
}
//int main()
//{
//	int input = 1;
//	int x, y;
//	int ret = 0;
//	do
//	{
//		
//		menu();
//		scanf("%d", &input);
//		switch (input)
//		{
//		case 1:
//			printf("请输入操作数：\n");
//			scanf("%d%d", &x, &y);
//			ret = Add(x, y);
//			printf("ret == %d\n", ret);
//			break;
//		case 2:
//			printf("请输入操作数：\n");
//			scanf("%d%d", &x, &y);
//			ret = Sub(x, y);
//			printf("ret == %d\n", ret);
//			break;
//		case 3 :
//			printf("请输入操作数：\n");
//			scanf("%d%d", &x, &y);
//			ret = Mul(x, y);
//			printf("ret == %d\n", ret);
//			break;
//		case 4:
//			printf("请输入操作数：\n");
//			scanf("%d%d", &x, &y);
//			ret = Div(x, y);
//			printf("ret == %d\n", ret);
//			break;
//		case 0:
//			printf("结束程序\n");
//			break;
//		default:
//			printf("选择错误，请重新选择\n");
//			break;
//		}
//	} while (input);
//
//	return 0;
//}

//int main()
//{
//	int input = 1;
//	int x, y;
//	int ret = 0;
//	int (*p[5])(int, int) = { 0, Add, Sub, Mul, Div };
//	do
//	{
//
//		menu();
//		scanf("%d", &input);
//		if (input <= 4 && input >= 1)
//		{
//			printf("请输入操作数：\n");
//			scanf("%d%d", &x, &y);
//			ret = (*p[input])(x, y);
//			printf("ret == %d\n", ret);
//		}
//		else if (input == 0)
//		{
//			printf("结束程序\n");
//		}
//		else
//		{
//			printf("选择错误，请重新选择\n");
//		}
//	} while (input);
//
//	return 0;
//}
void calc(int (*pf)(int, int))
{
	int x, y;
	int ret = 0;
	printf("请输入操作数：\n");
	scanf("%d%d", &x, &y);
	ret = pf(x, y);
	printf("ret == %d\n", ret);

}
int main()
{
	int input = 1;
	
	do
	{
		
		menu();
		scanf("%d", &input);
		switch (input)
		{
		case 1:
			calc(Add);
			break;
		case 2:
			calc(Sub);
			break;
		case 3 :
			calc(Mul);
			break;
		case 4:
			calc(Div);
			break;
		case 0:
			printf("结束程序\n");
			break;
		default:
			printf("选择错误，请重新选择\n");
			break;
		}
	} while (input);

	return 0;
}