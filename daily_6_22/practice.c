#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

int main()
{
	int arr[] = { 1,2,3,4,5,6,8,9,10,11,12,13,14 };
	int key = 12;
	int left = 0;
	int right = sizeof(arr) / sizeof(arr[0]) - 1;
	int mid = 0;
	int flag = 0;
	while (left <= right)
	{
		mid = left + (right - left) / 2;
		if (key > arr[mid])
		{
			left = mid + 1;
		}
		else if (key < arr[mid])
		{
			right = mid - 1;
		}
		else
		{
			flag = 1;
			break;
		}		
	}
	if (1 == flag)
	{
		printf("找到了%d\n", arr[mid]);
	}
	else
	{
		printf("找不到\n");
	}
	return 0;
}
//int main()
//{
//	char arr1[] = "hello world...";
//	char arr2[] = "##############";
//	int left = 0;
//	int right = strlen(arr1) - 1;
//	printf("%s\n", arr2);
//	while (left <= right)
//	{
//		Sleep(500);
//		system("cls");
//		arr2[left] = arr1[left];
//		arr2[right] = arr1[right];
//		printf("%s\n", arr2);
//		left++;
//		right--;
//	}
//	return 0;
//}
//int main()
//{
//	int arr[5] = { 1 };
//	return 0;
//}
//int main()
//{
//	srand((unsigned int)time(NULL));
//	printf("%d\n", rand());
//	rand() % 100;//0~99
//	rand() % 100 + 1;// 1~100
//	1 + rand() % (100 - 1 + 1);
//	//137~246
//	137 + rand() % (109 + 1);
//	// a + rand()%(b-a+1)
//	// a ~ b
//	return 0;
//}

//int main()
//{
//	//float a = 4.2f;
//	//printf("%lf", a);
//	printf("hehe\n");
//	goto next;
//	printf("haha\n");
//	
//next:
//	printf("跳过了haha的打印\n");
//	return 0;
//}

//int main()
//{
//	int n = 1;
//	int count = 0;
//	do
//	{
//		count++;
//		n /= 10;
//	} while (n);
//	printf("%d\n", count);
//	return 0;
//}

//int main()
//{
//	int sum = 0;
//	int i = 0;
//	for (i = 100; i <= 200; i++)
//	{
//		int flag = 1;
//		int j = 0;
//		for (j = 2; j < i; j++)
//		{
//			if (i % j == 0)
//			{
//				flag = 0;
//				break;
//			}
//		}
//		if (flag)
//		{
//			printf("%d ", i);
//			sum += i;
//		}
//	}
//	printf("\n", sum);
//	printf("相加的和是%d", sum);
//	return 0;
//}