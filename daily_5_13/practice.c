#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
//实现一个函数，可以左旋字符串中的k个字符。
//
//例如：
//strcpy strcat strncat strstr strlen
//ABCD左旋一个字符得到BCDA
//
//ABCD左旋两个字符得到CDAB
//void Rot(char ch[], int k)
//{
//	int len = strlen(ch);
//	int time = k % len;
//	//ABCDE
//	int i = 0;
//	for (i = 0; i < time; i++)
//	{
//		char tmp = ch[0];//CDEAB
//		int j = 0;
//		for (j = 0; j < len -1; j++)
//		{
//			ch[j] = ch[j+1];
//		}
//		ch[j] = tmp;
//	}
//}
//void Rot(char ch[], int k)
//{
//	int len = strlen(ch);
//	int time = k % len;
//	char cha[256] = { 0 };
//	strcpy(cha, ch);//ABCD
//	strncat(cha, ch, time);//3 ABCDABC
//	strcpy(ch, cha + time);
//	printf("%s\n", ch);
//}
//int main()
//{
//	char ch[] = "ABCDE";
//	int k = 0;
//	scanf("%d", &k);
//	Rot(ch, k);
//	printf("%s\n", ch);
//
//	return 0;
//}
//写一个函数，判断一个字符串是否为另外一个字符串旋转之后的字符串。
//
//例如：给定s1 = AABCD和s2 = BCDAA，返回1
//
//给定s1 = abcd和s2 = ACBD，返回0.
//
//AABCD左旋一个字符得到ABCDA
//
//AABCD左旋两个字符得到BCDAA
//
//AABCD右旋一个字符得到DAABC
int Rot(char ch[], char cha[])
{
	int len = strlen(ch);
	//int time = k % len;
	//ABCDE
	int i = 0;
	for (i = 0; i < len - 1; i++)
	{
		char tmp = ch[0];//CDEAB
		int j = 0;
		for (j = 0; j < len - 1; j++)
		{
			ch[j] = ch[j + 1];
		}
		ch[j] = tmp;
		if (strcmp(ch, cha) == 0)
		{
			return 1;
		}
	}
	return 0;
}
int main()
{
	char ch[] = "ABCDE";
	char cha[] = "CDEAB";
	int k = 0;
	//scanf("%d", &k);
	int ret = Rot(ch,cha);
	if (ret == 1)
		printf("YES\n");
	else
		printf("NO\n");

	return 0;
}