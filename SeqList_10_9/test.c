#define _CRT_SECURE_NO_WARNINGS 1
#include "SeqList.h"

void test()
{
	SeqList s1;
	SeqListInit(&s1);
	SeqListPushBack(&s1, 1);
	SeqListPushBack(&s1, 2);
	SeqListPushBack(&s1, 3);
	SeqListPushBack(&s1, 4);
	SeqListPushBack(&s1, 5);
	SeqListPushFront(&s1, 99);
	SeqListPushFront(&s1, 88);
	SeqListPrint(&s1);
	//SeqListPopFront(&s1);
	SeqListInsert(&s1, 4, 77);
	SeqListPrint(&s1);
	SeqListErase(&s1, 4);
	SeqListPrint(&s1);
	/*int ret = SeqListFind(&s1, 99);
	printf("%d\n", ret);*/
}
int main()
{
	test();
	return 0;
}