#define _CRT_SECURE_NO_WARNINGS 1
#include "SeqList.h"

//��ʼ��
void SeqListInit(SeqList* ps)
{
	ps->arr = NULL;
	ps->size = ps->capacity = 0;
}
//����
void SeqListDestroy(SeqList* ps)
{
	assert(ps);
	if (ps->arr);
		free(ps->arr);
	ps->arr = NULL;
	ps->size = ps->capacity = 0;
}
//��ӡ
void SeqListPrint(SeqList* ps)
{
	assert(ps);
	for (int i = 0; i < ps->size; i++)
	{
		printf("%d ", ps->arr[i]);
	}
	printf("\n");
}
//��������Ƿ��㹻
void SLcheckCapacity(SeqList* ps)
{	
	if (ps->size == ps->capacity)
	{
		int newCapacity = ps->capacity == 0 ? 4 : ps->capacity * 2;
		SLDateType* tmp = (SLDateType*)realloc(ps->arr, sizeof(SeqList) * newCapacity);
		if (tmp == NULL)
		{
			perror("realloc fail!");
			exit(1);
		}
		ps->arr = tmp;		
		ps->capacity = newCapacity;
	}	
}
//β��
void SeqListPushBack(SeqList* ps, SLDateType x)
{
	assert(ps);
	SLcheckCapacity(ps);
	ps->arr[ps->size++] = x;
	//ps->size++;
}
//ͷ��
void SeqListPushFront(SeqList* ps, SLDateType x)
{
	assert(ps);
	SLcheckCapacity(ps);
	for (int i = ps->size; i > 0; i--)
	{
		ps->arr[i] = ps->arr[i - 1];
	}
	ps->arr[0] = x;
	ps->size++;
}

//βɾ
void SeqListPopBack(SeqList* ps)
{
	assert(ps && ps->size);
	ps->size--;
}
//ͷɾ
void SeqListPopFront(SeqList* ps)
{
	assert(ps && ps->size);
	for (int i = 0; i < ps->size - 1; i++)
	{
		ps->arr[i] = ps->arr[i + 1];
	}
	ps->size--;
}

// ˳�������
int SeqListFind(SeqList* ps, SLDateType x)
{
	assert(ps);
	for (int i = 0; i < ps->size; i++)
	{
		if (x == ps->arr[i])
		{
			return i;
		}
	}
	return -1;
}

//˳�����posλ�ò���x
void SeqListInsert(SeqList* ps, int pos, SLDateType x)
{
	assert(ps);
	assert(pos >= 0 && pos < ps->size);
	SLcheckCapacity(ps);
	for (int i = ps->size; i > pos; i--)
	{
		ps->arr[i] = ps->arr[i - 1];
	}
	ps->arr[pos] = x;
	ps->size++;
}
// ˳���ɾ��posλ�õ�ֵ
void SeqListErase(SeqList* ps, int pos)
{
	assert(ps);
	assert(pos >= 0 && pos < ps->size);
	for (int i = pos; i < ps->size - 1; i++)
	{
		ps->arr[i] = ps->arr[i + 1];
	}
	ps->size--;
}