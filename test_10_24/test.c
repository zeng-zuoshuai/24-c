#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

//使用折半查找法实现给定一个有序（非降序）数组A，可含有重复元素，
//求最大的i使得A[i]小于target，不存在则返回 - 1
//【输入形式】
//第一行：非降序数组A
//第二行：target
//【输出形式】
//
//2 4 6 7 8 8 9
//9
//【样例输入】
//
//位置 i 或者 - 1
//【样例输出】
//
//5
//int SearchA(int A[], int sz, int target)
//{
//	int low = 0, hight = sz - 1;
//	int mind;
//	int flag = 0;
//	while (low <= hight)
//	{
//		mind = (low + hight) / 2;
//		if (A[mind] = target)
//		{
//			flag = 1;
//			break;
//		}
//		else if (A[mind] < target)
//			low = mind + 1;
//		else
//			hight = mind - 1;
//	}
//	if (flag)
//	{
//		int i = mind;
//		for (i = mind; i >= 0; i--)
//		{
//			if (A[i] != target)
//				return i;
//		}
//	}
//	return -1;
//}
//int main()
//{
//	int A[10] = { 2,4,6,7,8,8,9 };
//	int target = 9;
//	int sz = sizeof(A) / sizeof(A[0]);
//	int ret = SearchA(A, sz, target);
//	if (ret != -1)
//		printf("%d\n", ret);
//	else
//		printf("%s\n", "hehe");
//	return 0;
//}
//#include <stdio.h>
//
//int binarySearch(int arr[], int n, int target) {
//    int low = 0;
//    int high = n - 1;
//    int ans = -1;
//
//    while (low <= high) {
//        int mid = low + (high - low) / 2;
//        if (arr[mid] < target) {
//            ans = mid;
//            low = mid + 1;
//        }
//        else {
//            high = mid - 1;
//        }
//    }
//
//    return ans;
//}
//
//int main() {
//    int arr[1024] = { 0 };
//    int n = 0;
//    do
//    {
//        scanf("%d", &arr[n++]);
//    } while (getchar() != '\n');
//    int target = 9;
//    scanf("%d", &target);
//    int result = binarySearch(arr, n, target);
//    printf("%d\n", result);
//
//    return 0;
//}
//#include <stdio.h>
//
//int binarySearch(int arr[], int n, int key) {
//    int low = 0;
//    int high = n - 1;
//
//    while (low <= high) {
//        int mid = low + (high - low) / 2;
//        if (arr[mid] == key) {
//            return mid;
//        }
//        else if (arr[mid] < key) {
//            low = mid + 1;
//        }
//        else {
//            high = mid - 1;
//        }
//    }
//
//    return -1;
//}
//
//void printSteps(int steps[], int count) {
//    if (count == 0) {
//        printf("no\n");
//        return;
//    }
//    for (int i = 0; i < count; i++) {
//        printf("%d", steps[i]);
//        if (i < count - 1) {
//            printf(",");
//        }
//    }
//    printf("\n");
//}
//
//int main() {
//    int n;
//    scanf("%d", &n);
//
//    int arr[n];
//    for (int i = 0; i < n; i++) {
//        scanf("%d", &arr[i]);
//    }
//
//    int key;
//    scanf("%d", &key);
//
//    int index = binarySearch(arr, n, key);
//    if (index == -1) {
//        printf("no\n");
//    }
//    else {
//        printf("%d\n", index);
//    }
//
//    int steps[100];
//    int stepCount = binarySearch(arr, n, key);
//    if (stepCount == -1) 
//    {
//        stepCount = 0;
//    }
//    printSteps(steps, stepCount);
//
//    return 0;
//}
//#define _CRT_SECURE_NO_WARNINGS  1
//#include <stdio.h>
//
//int BinSearch(int* arr, int k, int n, int* flag, int* num, int* count) {
//	int i = 0, j = n - 1;
//	int mid;
//	while (i <= j) {
//		mid = (i + j) / 2;
//		num[(*count)++] = arr[mid];
//		if (arr[mid] == k) {
//			return mid;
//		}
//		if (arr[mid] < k)
//			i = mid + 1;
//		else
//			j = mid - 1;
//	}
//	*flag = 0;
//	return 0;
//}
//
//int main() {
//	int arr[1000], n, i, num[100];
//	scanf("%d", &n);
//	for (i = 0; i < n; i++) 
//	{
//		scanf("%d", arr + i);
//		getchar();
//	}
//	int k, flag = 1, count = 0;
//	scanf("%d", &k);
//	int ans = BinSearch(arr, k, n, &flag, num, &count);
//	if (flag) {
//		printf("%d\n", ans);
//		for (i = 0; i < count; i++) {
//			if (i == 0)
//				printf("%d", num[i]);
//			else
//				printf(",%d", num[i]);
//		}
//	}
//	else {
//		printf("no\n");
//		for (i = 0; i < count; i++) {
//			if (i == 0)
//				printf("%d", num[i]);
//			else
//				printf(",%d", num[i]);
//		}
//	}
//	return 0;
//}
#include <stdio.h>

int sign(int x) {
    if (x > 0) {
        return 1;
    }
    else if (x < 0) {
        return -1;
    }
    else {
        return 0;
    }
}

int main() {
    int x;
    printf("Enter x: ");
    scanf("%d", &x);
    printf("sign(%d)=%d\n", x, sign(x));
    return 0;
}