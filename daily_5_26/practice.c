#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#define NDEBUG
#include<assert.h>
#include<string.h>
//int main()
//{
//	int a = 0;
//	int* p = &a;
//	p = NULL;
//	assert(p );
//	return 0;
//}
//实现一个对整形数组的冒泡排序 升序
//void BubbleSort(int arr[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz - 1; i++)
//	{
//		int j = 0;
//		for (j = 0; j < sz - 1 - i; j++)
//		{
//			if (arr[j] > arr[j + 1])
//			{
//				int tmp = arr[j];
//				arr[j] = arr[j + 1];
//				arr[j + 1] = tmp;
//			}
//		}
//	}
//}
//void Print(int* p, int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", *(p + i));
//	}
//	printf("\n");
//}
//int main()
//{
//	int arr[] = { 1,3,5,7,9,2,4,6,8,10 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	Print(arr, sz);
//	BubbleSort(arr, sz);
//	Print(arr, sz);
//	return 0;
//}
//模拟实现库函数strlen
//int my_strlen(char* p)
//{
//	/*int count = 0;
//	int i = 0;
//	while (*(p + i) != '\0')
//	{
//		count++;
//		i++;
//	}
//	return count;*/
//	char* p1 = p;
//	while (*p1++)
//	{
//		;
//	}
//	return p1 - p - 1;
//}
//int main()
//{
//	char ch[] = "AB";
//	int len = strlen(ch);
//	printf("%d\n", len);
//	int ret = my_strlen(ch);
//	printf("%d\n", ret);
//}
//调整数组使奇数全部都位于偶数前面。
//
//题目：
//
//输入一个整数数组，实现一个函数，
//
//来调整该数组中数字的顺序使得数组中所有的奇数位于数组的前半部分，
//
//所有偶数位于数组的后半部分。
//void Adj(int* p, int sz)
//{
//	int left = 0;
//	int right = sz - 1;
//	while (left < right)
//	{
//		while (*(p + left) % 2 == 1 && left < right)
//		{
//			left++;
//		}
//		while (*(p + right) % 2 == 0 && left < right)
//		{
//			right--;
//		}
//		if (left < right)
//		{
//			int tmp = *(p + left);
//			*(p + left) = *(p + right);
//			*(p + right) = tmp;
//		}
//	}
//}
//void Print(int* p, int sz)
//{
//	for (int i = 0; i < sz; i++)
//	{
//		printf("%d ", *(p + i));
//	}
//	printf("\n");
//}
//int main()
//{
//	int arr[] = { 1,1,1,1,1,1,1,8,9,1 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	Print(arr, sz);
//	Adj(arr, sz);
//	Print(arr, sz);
//	return 0;
//}
//日本某地发生了一件谋杀案，警察通过排查确定杀人凶手必为4个嫌疑犯的一个。
//以下为4个嫌疑犯的供词:
//A说：不是我。
//B说：是C。
//C说：是D。
//D说：C在胡说
//已知3个人说了真话，1个人说的是假话。
//现在请根据这些信息，写一个程序来确定到底谁是凶手。
//int main()
//{
//	char killer = 0;
//	for (killer = 'A'; killer < 'D'; killer++)
//	{
//		if ((killer != 'A') + (killer == 'C') + (killer == 'D') + (killer != 'D') == 3)
//		{
//			printf("%c\n", killer);
//		}
//	}
//	return 0;
//}
//void Pot(int arr[][5], int x,int y)
//{
//	int i = 0;
//	for (i = 0; i < x ; i++)
//	{
//		int j = 0;
//		for (j = 0; j < y; j++)
//		{
//			printf("%d ", arr[i][j]);
//		}
//		printf("\n");
//	}
//	
//}
//int main()
//{
//	int arr[3][5] = { {1,2,3,4,5},{3,4,5,6,7},{5,6,7,8,9} };
//	Pot(arr, 3, 5);
//	return 0;
//}
//写一个函数，判断一个字符串是否为另外一个字符串旋转之后的字符串。
//
//例如：给定s1 = AABCD和s2 = BCDAA，返回1
//
//给定s1 = abcd和s2 = ACBD，返回0.
//
//AABCD左旋一个字符得到ABCDA
//
//AABCD左旋两个字符得到BCDAA
//
//AABCD右旋一个字符得到DAABC

//int Rot(char* s1, char* s2)
//{
//	int k = strlen(s1);
//	int i = 0;
//	for (i = 0; i < k; i++)
//	{
//		int j = 0;
//		char tmp = *s1;
//		for (j = 0; j < k - 1; j++)
//		{
//			*(s1 + j) = *(s1 + j + 1);
//
//
//		}
//		*(s1 + j) = tmp;
//		if ( strcmp(s1, s2) == 0)
//			return 1;
//	}
//	return 0;
//}
//int main()
//{
//	char s1[] = "AABCd";
//	char s2[] = "BCDAA";
//	int ret = Rot(s1, s2);
//	printf("%d\n", ret);
//	return 0;
//}
//有一个数字矩阵，矩阵的每行从左到右是递增的，矩阵从上到下是递增的，
//请编写程序在这样的矩阵中查找某个数字是否存在。
//要求：时间复杂度小于O(N);
// 1 2 3 4 5
// 2 3 4 5 6
// 3 4 5 6 7
// 4 5 6 7 8
//int Seek(int arr[4][3], int x,int y, int key)
//{	
//	int i = 0;
//	int j = y - 1;
//		while (arr[i][j] < key && (i < x))
//		{
//			i++;
//		}
//		while (arr[i][j] > key && j >= 0)
//		{
//			j--;
//			if (arr[i][j] == key)
//			{
//				return 1;
//			}
//	    }
//		return 0;
//}
//int Seek(int arr[4][3], int x, int y, int key)
//{
//	int i = 0;
//	int j = y - 1;
//	while (j >= 0 && i < x)
//	{
//		if (arr[i][j] < key)
//		{
//			i++;
//		}
//		else if (arr[i][j] > key)
//		{
//			j--;
//		}
//		else
//		{
//			return 1;
//		}
//	}
//	return 0;
//}
//int main()
//{
//	int arr[][3] = { {1,3,5},{3,6,7},{5,7,9},{9,10,11} };
//	int key = 8;
//	int ret = Seek(arr, key, 4, 3);
//	printf("%d\n", ret);
//	return 0;
//}
//在屏幕上打印杨辉三角。
//
//1
//
//1 1
//
//1 2 1
//
//1 3 3 1
//void YangH(int n)
//{
//	int arr[30][30] = { 1 };
//	int i = 0;
//	for (i = 1; i < n; i++)
//	{
//
//		int j = 0;
//		arr[i][j] = 1;
//		for (j = 1; j <= i; j++)
//		{
//			/*if (j == 0)
//			{
//				arr[i][j] = 1;
//			}
//			else
//			{
//				arr[i][j] = arr[i - 1][j - 1] + arr[i - 1][j];
//			}*/
//			arr[i][j] = arr[i - 1][j - 1] + arr[i - 1][j];
//			
//		}
//	}
//	for (i = 0; i < n; i++)
//	{
//		int j = 0;
//		for (j = 0; j <= i; j++)
//		{
//			printf("%d ", arr[i][j]);
//		}
//		printf("\n");
//	}
//}
//void Print(int arr[][10], int n)
//{
//	int i = 0;
//	for (i = 0; i < n; i++)
//	{
//		int j = 0;
//		for (j = 0; j <= i; j++)
//		{
//			printf("%d ", arr[i][j]);
//		}
//		printf("\n");
//	}
//}
//1
//
//1 1
//
//1 2 1
//
//1 3 3 1
void YangH(int n)
{
	int arr[100] = { 1 };
	int i = 0;
	int j = 0;
	printf("1\n");
	for (i = 1; i < n; i++)
	{
		
		for (j = i; j > 0; j--)
		{
			arr[j] += arr[j - 1];
		}
		for (j = 0; j <= i; j++)
		{
			printf("%d ", arr[j]);
		}
		putchar('\n');
	}
	
}
int main()
{
	//int arr[10][10] = { 0 };
	YangH(6);
	//Print(arr, 5);
	return 0;
}