#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
//写一个函数，判断一个字符串是否为另外一个字符串旋转之后的字符串。
//
//例如：给定s1 = AABCD和s2 = BCDAA，返回1
//
//给定s1 = abcd和s2 = ACBD，返回0.
//
//AABCD左旋一个字符得到ABCDA
//
//AABCD左旋两个字符得到BCDAA
//
//AABCD右旋一个字符得到DAABC
//int Det(char* s1, char* s2)
//{
//	size_t len = strlen(s1);
//	//库函数方法
//	//拼接strcpy
//	char s3[256] = { 0 };
//	strcpy(s3, s1);
//	strcat(s3, s1);
//	//s3 AABCDAABCD 
//	//s2   BCDAA
//	/*int i = 0;
//	int count = 0;
//	for (int i = 1; i <= len; i++)
//	{
//		strncat(s3, s1, i);
//		if (s3 + i == s2)
//		{
//			count++;
//			return 1;
//		}
//	}
//	return 0;
//	*/
//	char* rett = strstr(s3, s2);
//	if (rett != NULL)
//		return 1;
//	else
//		return 0;
//}旋转法
//int Det(char s1[], char s2[])
//{
//	size_t len = strlen(s1);
//	//字符串交换
//	//A BCDE
//	int i = 0;
//	for (i = 0; i < len; i++)
//	{
//		char tmp = s1[0];
//		int j = 0;
//		for (j = 0; j < len - 1; j++)
//		{
//			s1[j] = s1[j + 1];
//		}
//		s1[j] = tmp;
//		if (strcmp(s1, s2) == 0)
//		{
//			return 1;
//		}
//	}
//		return 0;
//}
//int main()
//{
//	char s1[] = "AABCD";
//	char s2[] = "BCDAA";
//	int ret = Det(s1, s2);
//	if (ret==1)
//	{
//		printf("Yes\n");
//	}
//	else
//	{
//		printf("No\n");
//	}
//	return 0;
//}
//日本某地发生了一件谋杀案，警察通过排查确定杀人凶手必为4个嫌疑犯的一个。
//
//以下为4个嫌疑犯的供词:
//
//A说：不是我。
//
//B说：是C。
//
//C说：是D。
//
//D说：C在胡说
//
//已知3个人说了真话，1个人说的是假话。
//
//现在请根据这些信息，写一个程序来确定到底谁是凶手。
//int main()
//{
//	char killer;
//	for (killer = 'A'; killer <= 'D'; killer++)
//	{
//		if ((killer != 'A') + (killer == 'C') + (killer == 'D') + (killer != 'D') == 3)
//		{
//			printf("killer是%c\n", killer);
//		}
//	}
//	return 0;
//}
//杨辉三角
//   1 
//   1  1
//   1  2  1
//   1  3  3  1
//   1  4  6  4  1
//   1  5  10 10 5 1;

void YangH(int arr[][6],int n)
{
	
	int i = 0;
	
	for (int i = 0; i < n; i++)
	{
		int j = 0;
		for (int j = 0; j <= i; j++)
		{
			if ((j == 0) || (i == j))
			{
				arr[i][j] = 1;

			}

			else
			{
				arr[i][j] = arr[i - 1][j] + arr[i - 1][j - 1];
			}
			
		}
	}
	
}
void Print(int arr[][6], int n)
{
	int i = 0;
	for (int i = 0; i < n; i++)
	{
		int j = 0;
		for (j = 0; j <= i; j++)
		{
			printf("%d ", arr[i][j]);
		}
		printf("\n");
	}
}
int main()
{
	int n = 6;
	int arr[][6] = { 0 };
	YangH(arr, n);
	Print(arr, n);
	return 0;
}
//有一个数字矩阵，矩阵的每行从左到右是递增的，矩阵从上到下是递增的，
//请编写程序在这样的矩阵中查找某个数字是否存在。
//要求：时间复杂度小于O(N);
//int Find(int arr[][3], int x, int y, int key)
//{
//	int i = 0;
//	int j = y - 1;
//	while (i <= x && j >= 0)
//	{
//		if (arr[i][j] < key)
//		{
//			i++;
//		}
//		else if (arr[i][j] > key)
//		{
//			j--;
//		}
//		else
//		{
//			return 1;
//		}
//	}
//	return 0;
//}
//int main()
//{
//	int key = 11;
//	int arr[4][3] = { {1,2,3},{4,5,6},{7,8,9},{10,11,12} };
//	
//	int ret = Find(arr, 4, 3, 6);
//	if (ret == 1)
//	{
//		printf("yes\n");
//	}
//	else
//	{
//		printf("no\n");
//	}
//
//	return 0;
//}