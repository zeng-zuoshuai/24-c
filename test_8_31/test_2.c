#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
//void init(int arr[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		arr[i] = 0;
//	}
//}
//void print(int arr[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	printf("\n");
//}
//void reverse(int arr[], int sz)
//{
//	int left = 0;
//	int right = sz - 1;
//	while (left <= right)
//	{
//		int tmp = arr[left];
//		arr[left] = arr[right];
//		arr[right] = tmp;
//		left++;
//		right--;
//	}
//}
//int main()
//{
//	int arr[9] = { 1,3,5,6,7,8,1,3,8 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	reverse(arr, sz);
//	print(arr, sz);
//	init(arr, sz);
//	print(arr, sz);
//	return 0;
//}
//写一个二分查找函数
//
//功能：在一个升序数组中查找指定的数值，找到了就返回下标，找不到就返回 - 1.
// arr 是查找的数组
//left 数组的左下标
//right 数组的右下标
//key 要查找的数字
//int bin_search(int arr[], int left, int right, int key)
//{
//	int mid = 0;
//	int find = 0;
//	while (left <= right)
//	{
//		mid = (left + right) / 2;
//		if (arr[mid] < key)
//		{
//			left = mid + 1;
//		}
//		else if (arr[mid] > key)
//		{
//			right = mid - 1;
//		}
//		else
//		{
//			find = 1;
//			break;
//		}	
//	}
//	if (1 == find)
//	{
//		return mid;
//	}
//	return -1;
//}
//int main()
//{
//	int arr[] = { 1,3,5,7,10,13,15,17 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int ret = bin_search(arr, 0, sz - 1, 10);
//	printf("%d\n", ret);
//	return 0;
//}
//int is_leap_year(int n)
//{
//	if (((n % 4 == 0) && (n % 100 != 0)) || (n % 400 == 0))
//		return 1;
//	else
//		return 0;
//}
//int main()
//{
//	int year = 0;
//	scanf("%d", &year);
//	int ret = is_leap_year(year);
//	printf("%d ", ret);
//	return 0;
//}
//实现一个函数is_prime，判断一个数是不是素数。
//
//利用上面实现的is_prime函数，打印100到200之间的素数。

//int is_prime(int n)
//{
//	int i = 0;
//	for (i = 2; i <= n - 1; i++)
//	{
//		if (n % i == 0)
//			return 0;
//	}
//	return 1;
//}
//int main()
//{
//	int i = 0;
//	for (i = 100; i <= 200; i++)
//	{
//		if (is_prime(i))
//			printf("%d ", i);
//	}
//	return 0;
//}
//喝汽水，1瓶汽水1元，2个空瓶可以换一瓶汽水，给20元，可以喝多少汽水（编程实现）。
//int main()
//{
//	int soda = 0;
//	int bottle = 0;
//	int money = 10;
//	while (money)
//	{
//		money--;
//		soda++;
//		bottle++;
//	}
//	while (bottle > 1)
//	{
//		bottle -= 2;
//		soda++;
//		bottle++;
//	}
//	printf("%d", soda);
//	return 0;
//}
//int main()
//{
//	int n = 7;
//	for (n = 7; n > 0; n--)
//	{
//		int i = n;
//		for (; i > 1; i--)
//		{
//			printf(" ");
//		}
//		for (i = (8 - n) * 2 - 1; i > 0; i--)
//		{
//			printf("*");
//		}
//		printf("\n");
//	}
//	for (n = 6; n > 0; n--)
//	{
//		int i = 8 - n - 1;
//		for (; i > 0; i--)
//		{
//			printf(" ");
//		}
//		for (i = n * 2 - 1; i > 0; i--)
//		{
//			printf("*");
//		}
//		printf("\n");
//	}
//	return 0;
//}
//求出0～100000之间的所有“水仙花数”并输出。
//
//“水仙花数”是指一个n位数，其各位数字的n次方之和确好等于该数本身，
//如 : 153＝1 ^ 3＋5 ^ 3＋3 ^ 3，则153是一个“水仙花数”。
//#include <math.h>
//int Narc(int num)
//{
//	int n = num;
//	int m = num;
//	int count = 0;
//	do
//	{
//		count++;
//		n /= 10;
//	} while (n);
//	int sum = 0;
//	while (m)
//	{
//		sum += pow(m % 10, count);
//		m /= 10;
//	}
//	if (sum == num)
//		return 1;
//	else
//		return 0;
//}
//int main()
//{
//	int i = 0;
//	for (i = 0; i <= 100000; i++)
//	{
//		if (Narc(i))
//			printf("%d ", i);
//	}
//
//	return 0;
//}
//求Sn = a + aa + aaa + aaaa + aaaaa的前5项之和，其中a是一个数字，
//
//例如：2 + 22 + 222 + 2222 + 22222
//
int main()
{
	int a = 0;
	scanf("%d", &a);
	int n = 5;
	int n2 = n;
	int Sn = 0;
	int i = 1;
	for (i = 1; i <= n; i++)
	{
		Sn += a * n2;
		n2--;
		a *= 10;
	}
	printf("%d ", Sn);
	return 0;
}