#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <assert.h>
#include <string.h>

int my_strcmp(const char* str1, const char* str2)
{
	assert(str1 && str2);
	while (*str1 == *str2)
	{
		if (*str1 == '\0')
			return 0;
		str1++;
		str2++;
	}
	return *str1 - *str2;
}

char* my_strstr(const char* str1, const char* str2)
{
	assert(str1 && str2);

	if (*str2 == '\0')
		return (char*)str1;

	char* s1 = (char*)str1;
	char* s2 = (char*)str2;

	char* cur = (char*)str1;
	while (*cur)
	{
		s1 = cur;
		s2 = (char*)str2;
		while (*s1 && *s2 && *s1 == *s2)
		{			
			s1++;
			s2++;
		}
		if (*s2 == '\0')
			return cur;
		cur++;
	}
	return NULL;
}

char* my_strnpy(char* dest, const char* src, size_t num)
{
	assert(dest && src);
	char* ret = dest;
	while (num-- && *src)
	{
		*dest++ = *src++;
	}
	*dest = '\0';
	return ret;
}

char* my_strncat(char* dest, const char* src, size_t num)
{
	char* ret = dest;
	assert(dest && src);
	while (*dest)
	{
		dest++;
	}
	while (num-- && *src)
	{
		*dest++ = *src++;
	}
	*dest = '\0';
	return ret;
}
//int main()
//{
//	char* arr = "abbbcde";
//	char* arr2 = "bbc";
//	//char* r = ;
//	printf("%s\n", my_strstr(arr, arr2));
//	return 0;
//}

//int main()
//{
//	char arr[] = "abcdef";
//	char arr2[30] = "xxxxx";
//	char* ret = my_strncat(arr2, arr, 4);
//	printf("%s\n", ret);
//	return 0;
//}

void* my_memcpy(void* dest, const void* src, size_t num)
{
	assert(dest && src);
	void* ret = (char*)dest;
	while (num--)
	{
		*(char*)dest = *(char*)src;
		dest = (char*)dest + 1;
		src = (char*)src + 1;
		//((char*)dest)++;
		//((char*)src)++;
	}
	return ret;
}

void* my_memmove(void* dest, const void* src, size_t num)
{
	void* ret = dest;
	assert(dest && src);

}
int main()
{
	int arr[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
	int arr2[30] = { 0 };
	my_memmove(arr + 2, arr, 5 * sizeof(arr[0]));
	return 0;
}