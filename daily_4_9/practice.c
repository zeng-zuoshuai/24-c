#define _CRT_SECURE_NO_WARNINGS 1
//递归和非递归分别实现求第n个斐波那契数
//例如：
//输入：5  输出：5
//输入：10， 输出：55
//输入：2， 输出：1
#include<stdio.h>
//int Fib(int n)
//{
//	if (n <= 2)
//		return 1;
//	else
//		return Fib(n - 1) + Fib(n - 2);
//}
//int main()
//{
//	int n = 0;
//	while (scanf("%d", &n) != EOF)
//	{
//		int r = Fib(n);
//		printf("%d\n", r);
//	}
//	return 0;
//}
//int main()
//{
//	int n = 0;
//	while (scanf("%d", &n) != EOF)
//	{
//		int a = 1;
//		int b = 1;
//		int c = 2;
//		while (n > 2)
//		{
//			c = a + b;
//			a = b;
//			b = c;
//			n--;
//		}
//		printf("%d\n", b);
//	}
//	return 0;
//}编写一个函数实现n的k次方，使用递归实现。
//int Pow(int n, int k)
//{
//	int sum = 0;
//	if (k == 1)
//		return n;
//	else
//		return n * Pow(n,k-1);
//
//}
//int main()
//{
//	int n = 0;
//	int k = 0;
//	while (scanf("%d%d", &n, &k) != EOF)
//	{
//		int r = Pow(n, k);
//		printf("%d\n", r);
//	}
//	return 0;
//}写一个递归函数DigitSum(n)，输入一个非负整数，返回组成它的数字之和
//例如，调用DigitSum(1729)，则应该返回1 + 7 + 2 + 9，它的和是19
//输入：1729，输出：19
//int DigitSum(int n)
//{
//	int sum = 0;
//	if (n / 10 == 0)
//		return (n % 10);
//	else
//	    return (n % 10)+DigitSum(n / 10);
//}
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int r = DigitSum(n);
//	printf("%d\n", r);
//
//	return 0;
//}递归和非递归分别实现求n的阶乘（不考虑溢出的问题）
//int Fac(int n)
//{
//	if (n==0)
//		return 1;
//	else
//		return n * Fac(n - 1);
//}
//int Fac(int n)
//{
//	int sum = 1;
//	while (n > 0)
//	{
//		sum *= n;
//		n--;
//	}
//	return sum;
//}
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int r = Fac(n);
//	printf("%d\n", r);
//	return 0;
//}递归方式实现打印一个整数的每一位
//void Sum(int n)顺序
//{
//	if (n / 10 == 0)
//		printf("%d ", n);
//	else
//	{
//		Sum(n / 10);
//		printf("%d ", n % 10);
//	}
//}
//void Sum(int n)逆序
//{
//
//	printf("%d ", n % 10);
//	while (n / 10 > 0)
//	{
//		n /= 10;
//		printf("%d ", n % 10);
//	}
//}
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	Sum(n);
//	
//	return 0;
//}