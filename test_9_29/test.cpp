#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <ctype.h>
int main()
{
	char arr[1000] = { 0 };
	int i = 0;
	int letter = 0;
	int blank = 0;
	int digit = 0;
	int other = 0;
	for (i = 0; i < 15; i++)
	{
		scanf("%c", &arr[i]);
		if (isalpha(arr[i]))
			letter++;
		else if (isspace(arr[i]))
			blank++;
		else if (isdigit(arr[i]))
			digit++;
		else
			other++;
	}
	printf("letter=%d blank=%d digit=%d other=%d", letter, blank, digit, other);
	return 0;
}