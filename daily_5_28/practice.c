#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
int pf(const int* p1, const int* p2)
{
	return *p1 - *p2;
}
void Print(int arr[], int sz)
{
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");
}
int main()
{
	int arr[] = { 1,2,3,4,1,6,7,8,9,10 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	Print(arr, sz);
	qsort(arr, sz, sizeof(arr[0]), pf);
	Print(arr, sz);
	return 0;
}
//int pf(const char* p1, const char* p2)
//{
//	return *p2 - *p1;
//}
//void Print(char arr[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%c ", arr[i]);
//	}
//	printf("\n");
//}
//int main()
//{
//	char ch[] = "ABCDEFG";
//	int sz = sizeof(ch) / sizeof(ch[0]);
//	Print(ch, sz);
//	qsort(ch, sz, sizeof(ch[0]), pf);
//	Print(ch, sz);
//	return 0;
//}