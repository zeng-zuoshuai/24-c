#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<math.h>
//求Sn=a+aa+aaa+aaaa+aaaaa的前5项之和，其中a是一个数字，
//例如：2 + 22 + 222 + 2222 + 22222
int main()
{
	int a = 0;
	while(scanf("%d", &a)!=EOF)
	{
		int k = pow(10, 4);
		int i = 1;
		int Sn = 0;
		while (i <= 5)
		{
			Sn += a * k * i;
			k /= 10;
			i++;
		}
		printf("%d\n", Sn); 
	}
	return 0;
}
//void num(int i, int n)
//{
//	int cmp = i;
//	int sn = 0;
//	while (cmp)
//	{
//		sn += pow(cmp % 10,n);
//		cmp = cmp / 10;
//	}
//	if (sn == i)
//		printf("%d ", sn);
//
//}
//int main()
//{
//	int i = 0;
//	for (i = 0; i <= 10000; i++)
//	{
//
//		int n = 0;
//		int cmp = i;
//		//cmp = i;
//		while (cmp)
//		{
//			cmp /= 10;
//			n++;
//		}
//
//	num(i, n);
//		
//
//	}
//
//	return 0;
//}