#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <stdbool.h>

bool verifyPostorder(int* postorder, int postorderSize) {
    if (postorderSize <= 1) {
        return true;
    }

    int rootValue = postorder[postorderSize - 1];
    int i = 0;
    while (i < postorderSize - 1 && postorder[i] < rootValue) {
        i++;
    }
    for (int j = i; j < postorderSize - 1; j++) {
        if (postorder[j] < rootValue) {
            return false;
        }
    }

    return verifyPostorder(postorder, i) && verifyPostorder(postorder + i, postorderSize - i - 1);
}

int main() {
    int postorder[] = { 5, 7, 6, 9, 11, 10, 8 };
    int postorderSize = sizeof(postorder) / sizeof(postorder[0]);
    bool result = verifyPostorder(postorder, postorderSize);
    printf(result ? "true\n" : "false\n");
    return 0;
}
//#include <stdio.h>
//#include <stdbool.h>

//void dfs(int node, int n, int graph[][n], bool visited[]) {
//    visited[node] = true;
//    for (int neighbor = 0; neighbor < n; neighbor++) {
//        if (graph[node][neighbor] == 1 && !visited[neighbor]) {
//            dfs(neighbor, n, graph, visited);
//        }
//    }
//}
//
//int countComponents(int n, int graph[][n]) {
//    bool visited[n];
//    for (int i = 0; i < n; i++) {
//        visited[i] = false;
//    }
//    int count = 0;
//    for (int i = 0; i < n; i++) {
//        if (!visited[i]) {
//            dfs(i, n, graph, visited);
//            count++;
//        }
//    }
//    return count;
//}
//
//int main() {
//    int n;
//    scanf("%d", &n);
//    int graph[n][n];
//    for (int i = 0; i < n; i++) {
//        for (int j = 0; j < n; j++) {
//            scanf("%d", &graph[i][j]);
//        }
//    }
//    int result = countComponents(n, graph);
//    printf("%d\n", result);
//    return 0;
//}
//struct ListNode {
//    int val;
//    struct ListNode *next;
//};
// 
//typedef struct ListNode ListNode;
//ListNode* BuySListNode(int x)
//{
//    ListNode* node = (ListNode*)malloc(sizeof(ListNode));
//    if (node == NULL)
//    {
//        perror("malloc fail!\n");
//        exit(1);
//    }
//    node->val = x;
//    node->next = NULL;
//    return node;
//}
//ListNode* removeElements(struct ListNode* head, int val)
//{
//    ListNode* pcur = head;
//    while (pcur)
//    {
//        ListNode* pnext = pcur->next;
//        //ɾ���ڵ�
//        if (pcur->val == val)
//        {
//            if (pcur == head)
//            {
//                ListNode* next = pcur->next;
//                ListNode* pos = pcur;
//                free(pos);
//                pos = NULL;
//                head = next;
//            }
//            else
//            {
//                //��prev
//                ListNode* prev = head;
//                ListNode* pos = pcur;
//                while (prev->next != pos)
//                {
//                    prev = prev->next;
//                }
//                prev->next = pos->next;
//                free(pos);
//                pos = NULL;
//            }
//        }
//        pcur = pnext;
//    }
//    return head;
//}
//
//int main()
//{
//    ListNode* l1 = BuySListNode(1);
//    ListNode* l2 = BuySListNode(2);
//    ListNode* l3 = BuySListNode(6);
//    ListNode* l4 = BuySListNode(3);
//    ListNode* l5 = BuySListNode(4);
//    ListNode* l6 = BuySListNode(5);
//    ListNode* l7 = BuySListNode(6);
//    l1->next = l2;
//    l2->next = l3;
//    l3->next = l4;
//    l4->next = l5;
//    l5->next = l6;
//    l6->next = l7;
//
//    removeElements(l1, 6);
//    return 0;
//}