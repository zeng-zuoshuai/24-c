#define _CRT_SECURE_NO_WARNINGS 1
#include "SeqList.h"

void SLInit(SL* ps)
{
	ps->arr = NULL;
	ps->size = ps->capacity = 0;
}
void SLDestroy(SL* ps)
{
	if (ps->arr)
	{
		free(ps->arr);
	}
	ps->arr = NULL;
	ps->size = ps->capacity = 0;
}
void SLPrint(SL* ps)
{
	int i = 0;
	for (i = 0; i < ps->size; i++)
	{
		printf("%d", ps->arr[i]);
	}
	printf("\n");
}
void SLCheckCapacity(SL* ps)
{
	if (ps->capacity == ps->size)
	{
		//����ռ�
		int NewCapacity = ps->capacity == 0 ? 4 : 2 * ps->capacity;
		SLDateType* ptr = (SLDateType*)realloc(ps, NewCapacity * sizeof(SLDateType));
		assert(ptr);
		ps->arr = ptr;
		ps->capacity = NewCapacity;
	}
}
void SLPushBack(SL* ps, SLDateType x)
{
	assert(ps);
	SLCheckCapacity(ps);
	ps->arr[ps->size++] = x;
}
void SLPushFront(SL* ps, SLDateType x)
{
	assert(ps);
	SLCheckCapacity(ps);
	for (int i = ps->size; i > 0; i--)
	{   
		ps->arr[i] = ps->arr[i - 1];//arr[1] = arr[0]
	}
	ps->arr[0] = x;
	ps->size++;
}
void SLPopBack(SL* ps)
{
	assert(ps);
	assert(ps->size);//˳�����Ϊ��
	ps->size--;
}
void SLPopFront(SL* ps)
{
	assert(ps);
	assert(ps->size);
	for (int i = 0; i < ps->size - 1; i++)
	{
		//arr[0] = arr[1]
		ps->arr[i] = ps->arr[i + 1];//arr[size-2] = arr[size-1]
	}
	ps->size--;
}