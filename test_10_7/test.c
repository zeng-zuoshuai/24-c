#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#define N 4

#define Y(n) ((N+2)*n) /*这种定义在编程规范中是严格禁止的*/

int main()
{
	int z = (N + Y(5 + 1));
	//      (4 + ((4+2)*5+1)
	printf("%d\n", z);
	return 0;
}