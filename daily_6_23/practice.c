#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

//int get_leap(int y)
//{
//	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0))
//		return 1;
//	else
//		return 0;
//}
//int get_day(int y, int m)
//{
//	int days[] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
//	int day = days[m - 1];
//	if (get_leap(y) && m == 2)
//	{
//		day += 1;
//	}
//	return day;
//}
//int main()
//{
//	int y = 0;
//	int m = 0;
//	scanf("%d%d", &y, &m);
//	int d = get_day(y, m);
//	printf("%d ", d);
//	return 0;
//}

//int main()
//{
//	int day = 1;
//	int day[5] = { 55, 66, 66, 77,88 };
//	
//	printf("%s", day);
//
//	return 0;
//}

//int main()
//{
//	static n = 10;
//	extern;
//}

//int main()
//{
//
//	int i = 0;
//	;
//	;
//	;
//	int sum = 0;
//	for (i = 0; i < 100; i++)
//	{
//		sum += 0;
//	}
//
//	printf("%d ", sum);
//	return 0;
//}


//void Func(int n)
//{
//	//Func(131415) + printf(5)
//	//FUnc(13141) + printf(1) + printf(5)
//	if (n > 9)
//	{
//		Func(n / 10);
//		printf("%d ", n % 10);
//	}
//	else
//		printf("%d ", n);
//
//}
//void Func(int n)
//{
//	if (n > 9)
//		Func(n / 10);
//	printf("%d ", n % 10);
//}
//void Func(int n)
//{
//	if (n < 9)
//		printf("%d ", n % 10);
//	else
//	{
//		Func(n / 10);
//		printf("%d ", n % 10);
//	}
//}
//int main()
//{
//	int n = 131415;
//
//    Func(n);
//	/*while (n)
//	{
//		printf("%d ", n % 10);
//		n /= 10;
//	}*/
//
//
//	return 0;
//}

//int main()
//{
//	int n = 5;
//	int i = 0;
//	int ret = 1;
//	for (i = 1; i <= n; i++)
//	{
//		ret *= i;
//	}
//	printf("%d ", ret);
//	return 0;
//}

//int Fib(int n)
//{
//	if (n <= 2)
//	{
//		return 1;
//	}
//	else
//		return Fib(n - 1) + Fib(n - 2);
//}
int Fib(int n)
{
	int a = 1;
	int b = 1;
	int c = 1;
	while (n > 2)
	{
		c = a + b;
		a = b;
		b = c;
		n--;
	}
	return c;

}
//int main()
//{
//	int n = 5;
//	struct tag
//	{
//		int a;
//		int c;
//	}s2;
//	struct tag s = { .a = 4 };
//	s.c = 5;
//	//int r = Fib(n);
//	//printf("%d", r);
//	return 0;
//}

//struct Point
//{
//	int x;
//	int u;
//};
//int main()
//{
//	struct Point abc = { 3, 4 };
//	struct Point* ptr = &abc;
//	//ptr->x = 10;
//	//ptr->u = 20;
//	printf("%d  %d", ptr->x, ptr->u);
//
//
//	return 0;
//}
#include <string.h>
struct Stu
{
	char name[15];
	int age;
};
void print_stu(struct Stu s)
{
	printf("%s %d\n", s.name, s.age);

}
void set_stu(struct Stu* ps)
{
	strcpy(ps->name, "lisi");
	ps->age = 20;
}
int main()
{
	struct Stu s = { "zhangsan", 18 };
	print_stu(s);
	set_stu(&s);
	print_stu(s);

	return 0;
}