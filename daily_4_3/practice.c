#define _CRT_SECURE_NO_WARNINGS 1
//实现一个函数is_prime，判断一个数是不是素数。

//利用上面实现的is_prime函数，打印100到200之间的素数。
#include<stdio.h>
//int is_prime(int a)
//{
//	int i = 0;
//	for (i = 2; i < a; i++)
//	{
//		if (a % i == 0)
//		{
//			return 0;
//		}
//		else return 1;
//	}
//}
//int main()
//{
//	int a = 0;
//	for (a = 100; a <= 200; a++)
//	{
//		int r = is_prime(a);
//		if (r)
//		{
//			printf("%d ", a);
//		}
//	}
//	return 0;
//}
//实现函数判断year是不是闰年。
//void is_run_year()
//{
//	int year = 0;
//	while (scanf("%d", &year) != EOF)
//	{
//		if ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0))
//			printf("%d是闰年\n", year);
//		else
//			printf("%d不是闰年\n", year);
//	}
//}
//int main()
//{
//	is_run_year();
//	return 0;
//}
//实现一个函数，打印乘法口诀表，口诀表的行数和列数自己指定
//如：输入9，输出9 * 9口诀表，输出12，输出12 * 12的乘法口诀表。
//void nice()
//{
//	int n = 0;
//	while (scanf("%d", &n) != EOF)
//	{
//		int i = 0;
//		for (i = 1; i <= n; i++)
//		{
//			int j = 0;
//			for (j = 1; j <= i; j++)
//			{
//				printf("%d*%d=%2d ", j, i, i * j);
//			}
//			printf("\n");
//		}
//	}
//}
//int main()
//{
//	nice();
//	return 0;
//}
//创建一个整形数组，完成对数组的操作
//实现函数init() 初始化数组为全0
//实现print()  打印数组的每个元素
//实现reverse()  函数完成数组元素的逆置。
//要求：自己设计以上函数的参数，返回值。
void init(int arr[], int sz)
{
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		 arr[i] = 0;
	}
}
void print(int arr[], int sz)
{
	/*int i = 0;*/
	for (int i =0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");
}
void reverse(int arr[], int sz)
{
	int i = 0;
	int arr2[10] = { 0 };
	int cmp = 0;
	int j = 0;

	for (i = sz -1,j = 0;  i >= 0 && j <sz; i--, j++)
	{
		cmp = arr[i];
		arr2[j] = cmp;
	}
	for (j = 0; j < sz; j++)
	{
		arr[j] = arr2[j];
		//printf("%d ", arr[j]);
	}print(arr, sz);
}
int main()
{
	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	//init(arr,sz);
	print(arr,sz);
	reverse(arr,sz);
	return 0;
}
