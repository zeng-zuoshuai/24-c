#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
//打印杨辉三角
int main()
{
	int sum = 0;
	for (int i = 0; i < 5; i++)
	{
		sum += i;
	}
	//i = 4;
	printf("%d", sum);
	return 0;
}
//有一个整数序列（可能有重复的整数），现删除指定的某一个整数,
//输出删除指定数字之后的序列，序列中未被删除数字的前后位置没有发生改变。
//数据范围：序列长度和序列中的值都满足
//1≤n≤50
//输入描述：
//第一行输入一个整数(0≤N≤50)。
//第二行输入N个整数，输入用空格分隔的N个整数。
//第三行输入想要进行删除的一个整数。
//输出描述：
//输出为一行，删除指定数字之后的序列。

//int main()
//{
//	int N;
//	scanf("%d", &N);
//	int arr[5] = { 0 };
//	int i = 0;
//	for (i = 0; i < N; i++)
//	{
//		scanf("%d", &arr[i]);
//	}
//	int x = 0;
//	scanf("%d", &x);
//	i = 0;
//	int j = 0;
//	while (j < N)
//	{
//		if (arr[j] != x)
//		{
//			arr[i] = arr[j];
//			i++;
//		}
//
//		j++;
//	}
//	for (j = 0; j < i; j++)
//	{
//		printf("%d ", arr[j]);
//	}
//	return 0;
//}
//int main()
//{
//	int n, m;
//	scanf("%d%d", &n, &m);
//	int arr1[n + m];
//	int arr2[m];
//	int i = 0;
//	for (i = 0; i < n; i++)
//	{
//		scanf("%d", &arr1[i]);
//	}
//	for (i = 0; i < m; i++)
//	{
//		scanf("%d", &arr2[i]);
//	}
//	i = 0;
//	int s1 = n - 1;
//	int s2 = m - 1;
//	int s3 = n + m - 1;
//	while (s1 >= 0 && s2 >= 0)
//	{
//		if (arr2[s2] > arr1[s1])
//		{
//			arr1[s3] = arr2[s2];
//			s2--;
//		}
//		else
//		{
//			arr1[s3] = arr1[s1];
//			s1--;
//		}
//		s3--;
//	}
//	if (s1 < 0)
//	{
//		while (s2 >= 0)
//		{
//			arr1[s2] = arr2[s2];
//			s2--;
//		}
//	}
//	for (i = 0; i < n + m; i++)
//	{
//		printf("%d ", arr1[i]);
//	}
//	return 0;
//}
//int main()
//{
//	int a, b, c;
//	scanf("%d%d%d", &a, &b, &c);
//	if (b < a)
//	{
//		int tmp = b;
//		b = a;
//		a = tmp;
//	}
//	if (c < a)
//	{
//		int tmp = c;
//		c = a;
//		a = tmp;
//	}
//	if (c < b)
//	{
//		int tmp = c;
//		c = b;
//		b = tmp;
//	}
//	printf("%d %d %d", a, b, c);
//	return 0;
//}

//#include<stdio.h>
//#include<math.h>
//在此处编写dist函数
//float dist(float x1, float y1, float x2, float y2)
//{
//    float d2 = pow(x1 - x2, 2) + pow(y1 - y2, 2);
//    return pow(d2, 0.5);
//}
//int  main() {
//    float  xs, ys, xe, ye, d;
//    scanf("%f%f", &xs, &ys);
//    scanf("%f%f", &xe, &ye);
//    d = dist(xs, ys, xe, ye);
//    printf("Distance=%.2f", d);
//    return  0;
//}
//【问题描述】比较大小：输入三个整数，按从小到大顺序输出。
//【输入形式】三个整数，以单个空格分隔
//【输出形式】三个整数，以单个空格分隔，由小到大输出
//【样例输入】2 6 5
//【样例输出】2 5 6
//int main()
//{
//	int a, b, c;
//	scanf("%d %d %d", &a, &b, &c);
//	if (b < a)
//	{
//		int tmp = b;
//		b = a;
//		a = tmp;
//	}
//	if (c < a)
//	{
//		int tmp = c;
//		c = a;
//		a = tmp;
//	}
//	if (c < b)
//	{
//		int tmp = c;
//		c = b;
//		b = tmp;
//	}
//	printf("%d %d %d", a, b, c);
//	return 0;
//}

//int main()
//{
//	int a, b, c;
//	scanf("%d%d%d", &a, &b, &c);
//	if (b > a)
//	{
//		int tmp = b;
//		b = a;
//		a = tmp;
//	}
//	if (c > a)
//	{
//		int tmp = c;
//		c = a;
//		a = tmp;
//	}
//	if (c > b)
//	{
//		int tmp = c;
//		c = b;
//		b = tmp;
//	}
//	printf("%d %d %d", c, b, a);
//}

//【问题描述】输入一个正整数n，再输入n个整数，输出其中最小的值。
//【输入形式】先输入一个整数n，再根据n，输入n个数
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int i = 0;
//	int arr[n];
//	for (i = 0; i < n; i++)
//	{
//		scanf("%d", &arr[i]);
//	}
//	int min = arr[0];
//	for (i = 1; i < n; i++)
//	{
//		if (arr[i] < min)
//			min = arr[i];
//	}
//	printf("min=%d", min);
//	return 0;
//}
//【问题描述】输入一个正整数n（1 < n < 10），
//再输入n个整数，存入数组中，再将数组中的数，逆序存放并输出
//【输入形式】先输入一个整数n，再输入n个整数，用空格间隔
//【输出形式】输出n个整数，用空格间隔
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int arr[9] = {0};
//	int i = 0;
//	for (i = 0; i < n; i++)
//	{
//		scanf("%d", &arr[i]);
//	}
//	i = 0;
//	int j = n - 1;
//	while (i < j)
//	{
//		int tmp = arr[i];
//		arr[i] = arr[j];
//		arr[j] = tmp;
//		i++;
//		j--;
//	}
//	for (i = 0; i < n; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//
//	return 0;
//}
//【问题描述】求给定序列（1 + 1 / 2 + 1 / 3 + ……）前n项的和：
//输入一个正整数n，计算序列1 + 1 / 2 + 1 / 3 + ……的前n项之和；
//
//【输入形式】输入一个整数值，输出一个单精度浮点数。
//【输出形式】输出n的值，前面包含字符串"n="；输出逗号","；
//输出求和后的结果值，前面包含字符串"sum="，保留7位小数
//【样例输入】5
//【样例输出】n = 5, sum = 2.2833335

//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	float sum = 0;
//	int i = 1;
//	for (i = 1; i <= n; i++)
//	{
//		sum += 1.0 / i;
//	}
//	printf("n=%d,sum=%.7f", n, sum);
//}
//int main()
//{
//	//计算摄氏温度：输入华氏温度，输出对应的摄氏温度。
//	// 计算公式如下：   c=5×(f-32)÷9
//	//其中，c表示摄氏温度，f表示华氏温度，均使用浮点数存储数据。
//	double f = 0;
//	scanf("%lf", &f);
//	double c = (f - 32) * 5 / 9.0;
//	printf("Celsius=%.2f\n", c);
//
//	return 0;
//}
