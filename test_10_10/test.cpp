#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>

int main() {
    int n;
    printf("请输入正整数 n：");
    scanf("%d", &n);
    int num;
    int min;
    printf("请输入 %d 个整数：\n", n);
    scanf("%d", &num);
    min = num;
    for (int i = 1; i < n; i++) {
        scanf("%d", &num);
        if (num < min) {
            min = num;
        }
    }
    printf("min=%d\n", min);
    return 0;
}
