#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <string.h>
#include <assert.h>

void* my_memmove(void* dest, const void* src, size_t num)
{
	void* ret = dest;
	assert(dest && src);
	if (dest < src)
	{
		//前到后
		while (num--)
		{
			*(char*)dest = *(char*)src;
			dest = (char*)dest + 1;
			src = (char*)src + 1;
		}	
	}
	else
	{
		//后到前
		while (num--)
		{
			*((char*)dest + num) = *((char*)src + num);
		}
	}
	return ret;
}
//int main()
//{
//	int arr[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
//
//	my_memmove(arr, arr + 2, 5 * sizeof(int));
//
//	return 0;
//}

#include <stdio.h>

//写一个函数判断当前机器是大端还是小端，如果是小端返回1，如果是大端返回0

//int check_sys()
//{
//	int a = 1;
//	return *(char*)&a;
//}
//int main()
//{
//	printf("%d\n", check_sys());
//}
int main()
{                          //                         128 64 32 16 8 4 2 1
	unsigned char a = 200; //00000000 00000000 00000000 1  1  0  0 1 0 0 0
	unsigned char b = 100; //00000000 00000000 00000000 0  1  1  0 0 1 0 0
	unsigned char c = 0;   //                         1 0  0  1  0 1 1 0 0
	c = a + b;             // 256 32 12 = 300 44
	printf("%d %d", a + b, c);
	return 0;
}