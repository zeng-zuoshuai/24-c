#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

int main() {
    int n;
    scanf("%d", &n);
    char ch = 'A';
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n - i; j++) {
            printf("%c ", ch++);
        }
        printf("\n");
    }
    return 0;
}