#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>

//int main()
//{
//	int arr[10] = { 0 };
//	int i = 0;
//	int sum = 0;
//	for (i = 0; i < 10; i++)
//	{
//		scanf("%d", &arr[i]);
//		sum += arr[i];
//	}
//	printf("%f ", sum / 10.0);
//	return 0;
//}

//int main()
//{
//	int arr1[] = { 1, 2, 3, 4, 5 };
//	int arr2[] = { 6, 7, 8, 9, 10 };
//	int sz = sizeof(arr1) / sizeof(arr1[0]);
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		int tmp = arr1[i];
//		arr1[i] = arr2[i];
//		arr2[i] = tmp;
//	}
//	return 0;
//}
//一个数组中只有两个数字是出现一次，其他所有数字都出现了两次。
//
//编写一个函数找出这两个只出现一次的数字。
//
//例如：
//
//有数组的元素是：1，2，3，4，5，1，2，3，4，6
//
//只有5和6只出现1次，要找出5和6.
//void Find(int arr[], int sz, int* p1, int* p2)
//{
//	int sum = 0;
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		sum ^= arr[i];
//	}
//	int pose = 0;
//	for (i = 0; i < 32; i++)
//	{
//		if (((sum >> i) & 1) == 1)
//		{
//			pose = i;
//			break;
//		}
//	}
//	for (i = 0; i < sz; i++)
//	{
//		if (((arr[i] >> pose) & 1) == 1)
//		{
//			*p1 ^= arr[i];
//		}
//		else
//		{
//			*p2 ^= arr[i];
//		}
//	}
//}
//int main()
//{
//	int arr[] = { 1, 2, 3, 4, 5, 1, 2, 3, 4, 6 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int a = 0;
//	int b = 0;
//	Find(arr, sz, &a, &b);
//	printf("%d %d", a, b);
//	return 0;
//}

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void game()
{
	//生成1~100的随机数
	int r = 1 + rand() % 100;
	int guess = 0;
	int count = 5;
	while (count)
	{
		printf("你还有%d次机会\n", count);
		printf("请猜数字:>");
		scanf("%d", &guess);
		if (guess < r)
			printf("猜小了\n");
		else if (guess > r)
			printf("猜大了\n");
		else
			printf("恭喜你猜对了!\n");
		count--;
	}
	if (count == 0)
	{
		printf("很遗憾，你失败了\n");
		printf("正确答案是%d\n", r);
	}
}
void menu()
{
	printf("**********************\n");
	printf("*****   1.play   *****\n");
	printf("*****   0.exit   *****\n");
	printf("**********************\n");
	printf("请选择:>");
}
int main()
{
	int input = 0;
	srand((unsigned int)time(NULL));	
	do
	{
		printf("*****猜数字小游戏*****\n");
		menu();
		scanf("%d", &input);
		switch (input)
		{
		case 1:
			game();
			break;
		case 0:
			printf("程序退出\n");
			break;
		default :
			printf("程序错误请重新选择\n");
			break;
		}
	} while (input);

	return 0;
}