#define _CRT_SECURE_NO_WARNINGS 1



//int main()
//{
//	const int n = 10;
//	int arr[n];
//	return 0;
//}


//输入一个字符串，可以有空格
//
//输出描述:
//输出逆序的字符串
//
//示例1
//输入
//I am a student
//输出
//tneduts a ma I
#include<stdio.h>
#include <string.h>
//int main()
//{
//	char arr[1000] = { 0 };
//	gets(arr);
//	int left = 0;
//	int right = strlen(arr) - 1;
//	while (left < right)
//	{
//		char tmp = arr[left];
//		arr[left] = arr[right];
//		arr[right] = tmp;
//		left++;
//		right--;
//	}
//	printf("%s\n", arr);
//	return 0;
//}
//模拟实现库函数strlen
//#include <assert.h>
//size_t my_strlen(char* str)
//{
//	assert(str);
//	char* p = str;
//	while (*p)
//		p++;
//	return p - str;	
//}
//int main()
//{
//	char str[] = "ABCDEF";
//	size_t len = my_strlen("");
//	printf("%zd", len);
//
//
//	return 0;
//}
void swap(int* p, int sz)
{
	int left = 0;
	int right = sz - 1;
	while (left < right)
	{
		while (*(p + left) % 2 == 1)
			left++;
		while (*(p + right) % 2 == 0)
			right--;
		if (left < right)
		{
			int tmp = *(p + left);
			*(p + left) = *(p + right);
			*(p + right) = tmp;
			left++;
			right--;
		}
	}
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%d ", *(p + i));
	}
}
int main()
{
	int arr[] = { 2,4,6,5,8,10,1,3,5,6,6,6 };

	int sz = sizeof(arr) / sizeof(arr[0]);

	swap(arr, sz);

	return 0;
}