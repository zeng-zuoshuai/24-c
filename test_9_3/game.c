#define _CRT_SECURE_NO_WARNINGS 1

#include "game.h"

//初始化棋盘
void InitBoard(char board[ROWS][COLS], int rows, int cols, char set)
{
	int i = 0;
	for (i = 0; i < rows; i++)
	{
		int j = 0;
		for (j = 0; j < cols; j++)
		{
			board[i][j] = set;
		}
	}
}

//打印棋盘
void DisplayBoard(char board[ROWS][COLS], int row, int col)
{
	int i = 0;
	printf("------扫雷游戏-----\n");
	for (i = 0; i <= col; i++)
	{
		printf("%d ", i);
	}
	printf("\n");
	for (i = 1; i <= row; i++)
	{
		printf("%d ", i);
		int j = 0;
		for (j = 1; j <= col; j++)
		{
			printf("%c ", board[i][j]);
		}
		printf("\n");
	}
}

//布置雷
void SetMine(char board[ROWS][COLS], int row, int col)
{
	//布置EASY_COUNT个雷
	//生成随机的坐标，布置雷
	int count = EASY_COUNT;
	while (count)
	{
		int x = rand() % row + 1;//生成1~row的随机数
		int y = rand() % col + 1;
		if (board[x][y] == '0')//如果这个坐标没布置过雷就布置
		{
			board[x][y] = '1';//布置雷
			count--;//布置成功后count-1
		}
	}
}

//排查雷
int GetMineCount(char mine[ROWS][COLS], int x, int y)
{
	return (mine[x - 1][y - 1] + mine[x - 1][y] + mine[x - 1][y + 1]
		+ mine[x][y - 1] + mine[x][y + 1] + mine[x + 1][y - 1]
		+ mine[x + 1][y] + mine[x + 1][y + 1] - 8 * '0');
}
void FindMine(char mine[ROWS][COLS], char show[ROWS][COLS], int row, int col)
{
	int x = 0;
	int y = 0;
	int win = 0;//win代表排查过的位置
	while (win < row * col - EASY_COUNT)//当排查过的位置等于row*col-雷的个数的时候，游戏就胜利了
	{
		printf("请输入要排查的坐标:>");
		scanf("%d%d", &x, &y);
		if (x >= 1 && x <= row && y >= 1 && y <= col)//如果坐标合法就进行排雷
		{
			if (mine[x][y] == '1')//是雷
			{
				printf("很遗憾，你被炸死了!\n");
				DisplayBoard(mine, ROW, COL);//展示布置雷的情况
				break;
			}
			else
			{
				//该位置不是雷，就统计这个坐标周围有几个雷
				int count = GetMineCount(mine, x, y);//排查这个坐标周围有几个雷
				show[x][y] = count + '0';//将雷的个数展示出来
				DisplayBoard(show, ROW, COL);//展示棋盘
				win++;//排查过的位置+1
			}
		}
		else
		{
			printf("坐标非法，重新输入\n");
		}
	}
	if (win == row * col - EASY_COUNT)
	{
		printf("恭喜你，排雷成功!\n");
		DisplayBoard(mine, ROW, COL);
	}
}