#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

//递归和非递归分别实现求第n个斐波那契数
//例如：
//输入：5  输出：5
//输入：10， 输出：55
//输入：2， 输出：1

//int Fib(int n)
//{
//	if (n <= 2)
//		return 1;
//	else
//		return Fib(n - 1) + Fib(n - 2);
//}
//int Fib(int n)
//{
//	int a = 1;
//	int b = 1;
//	int c = 1;
//	while (n > 2)
//	{
//		c = a + b;
//		a = b;
//		b = c;
//		n--;
//	}
//	return c;
//}
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//
//	int r = Fib(n);
//	printf("%d\n", r);
//	return 0;
//}
//编写一个函数实现n的k次方，使用递归实现。
//int Func(int n, int k)
//{
//	if (k == 1)
//		return n;
//	else
//		return n * Func(n, k - 1);
//}
//int main()
//{
//	int n = 0;
//	int k = 0;
//	scanf("%d%d", &n, &k);
//
//	int r = Func(n, k);
//	printf("%d\n", r);
//
//	return 0;
//}
//写一个递归函数DigitSum(n)，输入一个非负整数，返回组成它的数字之和
//
//例如，调用DigitSum(1729)，则应该返回1 + 7 + 2 + 9，它的和是19
//
//输入：1729，输出：19
//int DigitSum(int n)
//{
//	if (n > 9)
//		return DigitSum(n / 10) + n % 10;
//	else
//		return n;
//}
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//
//	int r = DigitSum(n);
//	printf("%d\n", r);
//
//	return 0;
//}
//递归和非递归分别实现求n的阶乘（不考虑溢出的问题）
//int Func(int n)
//{
//	if (n == 0)
//		return 1;
//	else
//		return n * Func(n - 1);
//}
//int Func(int n)
//{
//	int sum = 1;
//	while (n--)
//	{
//		sum *= n;
//		//n--;
//	}
//	return sum;
//}递归方式实现打印一个整数的每一位
//void Func(int n)
//{
//	if (n > 9)
//		Func(n / 10);
//	printf("%d ", n % 10);
//}
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//
//	Func(n);
//	
//
//	return 0;
//}
// 9^3 & 
// 8421
// 1100
// 0101
// 0100
// 1001
// 0011
// 1010 
//在一个整型数组中，只有一个数字出现一次，
//其他数组都是成对出现的，请找出那个只出现一次的数字。
//
//例如：
//
//数组中有：1 2 3 4 5 1 2 3 4，只有5出现一次，其他数字都出现2次，找出5

#include<stdio.h>

//int main()
//{
//	int arr[] = { 1, 2, 3, 4, 5, 1, 2, 3, 4 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int sum = 0;
//	// a ^ a = 0; a ^ 0 = a;
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		sum ^= arr[i];
//	}
//	printf("%d\n", sum);
//	return 0;
//}

//int  main()
//{
//	int a = 10;
//	int b = 20;
//
//	printf("交换前a==%d , b==%d\n", a, b);
//	a = a ^ b;
//	b = a ^ b;
//	a = a ^ b;
//	printf("交换后a==%d , b==%d\n", a, b);
//	return 0;
//}
//int  main()
//{
//	int a = 10;
//	int b = 20;
//
//	printf("交换前a==%d , b==%d\n", a, b);
//	a = a + b;
//	b = a - b;
//	a = a - b;
//	printf("交换后a==%d , b==%d\n", a, b);
//	return 0;
//}
//获取一个整数二进制序列中所有的偶数位和奇数位，分别打印出二进制序列

//int main()
//{
//	int n = 17;
//	//8421
//	//1101
//	int i = 0;
//	for (i = 31; i > 0; i --)
//	{
//		printf("%d", n & (1 << i));
//	}
//	printf("\n");
//	printf("奇数位：");
//	
//	for (i = 31; i > 0; i -= 2)
//	{
//		printf("%d", n & (1 << i));
//		
//	}
//	printf("\n");
//
//	printf("偶数位：");
//	for (i = 30; i > 0; i -= 2)
//	{
//		printf("%d", n & (1 << i));
//	}
//
//	return 0;
//}

//int main()
//{
//	int n = 18;
//	int i = 0;
//	for (i = 31; i >= 0; i--)
//	{
//		if ((n >> i) & 1)
//			printf("%d", 1);
//		else
//			printf("%d", 0);
//	}
//	printf("\n");
//	for (i = 31; i >= 0; i-= 2)
//	{
//		if ((n >> i) & 1)
//			printf("%d ", 1);
//		else
//			printf("%d ", 0);
//	}
//	printf("\n");
//	printf(" ");
//	for (i = 30; i >= 0; i -= 2)
//	{
//		if ((n >> i) & 1)
//			printf("%d ", 1);
//		else
//			printf("%d ", 0);
//	}
//	return 0;
//}
//输入两个整数，求两个整数二进制格式有多少个位不同

//int main()
//{
//	int a = 0;
//	int b = 0;
//	scanf("%d%d", &a, &b);
//
//	int i = 0;
//	int count = 0;
//	for (i = 0; i < 32; i++)
//	{
//		if (((a >> i) & 1) != ((b >> i) & 1))
//			count++;
//	}
//	printf("%d\n", count);
//	return 0;
//}
//实现一个函数，可以左旋字符串中的k个字符。
//
//例如：
//
//ABCD左旋一个字符得到BCDA
//0123 4
//ABCD左旋两个字符得到CDAB

void Func(char arr[], int sz, int k)
{
	int i = 0;
	for (i = 0; i < k; i++)
	{
		char tmp = arr[0];
		int j = 0;
		for (j = 0; j < sz - 1; j++)
		{
			arr[j] = arr[j + 1];
		}
		arr[j - 1] = tmp;
	}

}
int main()
{
	char arr[] = "ABCD";
	int k = 1;

	printf("%s\n", arr);
	int sz = sizeof(arr) / sizeof(arr[0]);
	Func(arr, sz, k);

	printf("%s\n", arr);

	return 0;
}